#ifndef FILE_H
#define FILE_H

#include <stdio.h>
#include "str.h"

enum FileMode
{
	FILEMODE_READ      = 1 << 0,
	FILEMODE_WRITE     = 1 << 1,
	FILEMODE_OVERWRITE = 1 << 2,
};

struct File
{
	struct Str path;
	struct Str content;
	FILE* raw;
	const char* extension; //NOTE: no memory is allocated
	enum FileMode mode;
};

struct File* file_ctor(
	struct File* self, 
	struct StrSlice path, 
	enum FileMode mode
);
int file_exists(const char* path);
void file_flush(struct File* self);
void file_dtor(struct File* self);

#endif
