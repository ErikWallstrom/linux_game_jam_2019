#ifndef GLWINDOW_H
#define GLWINDOW_H

#include "color.h"

enum GLWindowFlags
{
	GLWINDOW_DEFAULT	       = 0,
	GLWINDOW_VSYNC 	           = 1 << 0,
	GLWINDOW_RESIZABLE         = 1 << 1,
	GLWINDOW_FULLSCREEN        = 1 << 2,
	GLWINDOW_FULLSCREENDESKTOP = 1 << 3,
};

struct GLWindow
{
	struct Color color;
	void* raw;
	void* context;
	const char* title;
	int width, height;
	int flags;
};

struct GLWindow* glwindow_ctor(
	struct GLWindow* self, 
	const char* title, 
	int display, 
	int width, 
	int height,
	enum GLWindowFlags flags
);
void glwindow_setwidth(struct GLWindow* self, int width);
void glwindow_setheight(struct GLWindow* self, int height);
void glwindow_settitle(struct GLWindow* self, const char* title);
void glwindow_setclearcolor(struct GLWindow* self, struct Color color);
void glwindow_present(struct GLWindow* self);
void glwindow_dtor(struct GLWindow* self);

#endif
