#ifndef TILEMAP_H
#define TILEMAP_H

#include "../../texture.h"
#include "../../vec.h"

#define TILES_X 50
#define TILES_Y 50
#define TILE_WIDTH 250
#define TILE_HEIGHT 250
#define TILE_SCALE 1
#define TILE_NUMTYPES 4
#define MAP_WIDTH  (TILE_WIDTH  * TILE_SCALE * TILES_X)
#define MAP_HEIGHT (TILE_HEIGHT * TILE_SCALE * TILES_Y)

struct Game;

struct Tile
{
	int type;
	int rotation;
};

struct TileMap
{
	struct Texture tilestypes[TILE_NUMTYPES];
	Vec(struct Tile) map;
};

struct TileMap* tilemap_ctor(struct TileMap* self, struct Game* game);
void tilemap_render(struct TileMap* self, struct Game* game);
void tilemap_dtor(struct TileMap* self);

#endif
