#include "../../initialize.h"
#include "../../gameloop.h"
#include "../../color.h"
#include "../../log.h"
#include "game.h"
#include "player.h"
#include "setup.h"

#define XRESOLUTION 1600
#define YRESOLUTION 900
#define TICKSPERSEC 60

struct Game game;
int starttime;

void wave4(void)
{
	static int done = 0;
	if(!done)
	{
		texture_ctortext(&game.wave, &game.font, "Wave 4/4", &game.window);
		game.wavenum = 4;

		for(size_t i = 0; i < 50; i++)
		{
			struct Vec2d pos;
			while(1)
			{
				pos = (struct Vec2d){
					.x = rand() % MAP_WIDTH, 
					.y = rand() % MAP_HEIGHT
				};
				struct Vec2d diff;
				vec2d_sub(&game.player.player.unit.rect.pos, &pos, &diff);
				if(vec2d_length(&diff) >= 1200)
				{
					break;
				}
			}

			struct SoldierUnit testsoldier;
			soldierunit_ctor(
				&testsoldier, 
				pos,
				&game
			);
			testsoldier.isenemy = 1;
			testsoldier.command = SOLDIERCOMMAND_OFFENSIVE;
			log_info("Isdead: %i", testsoldier.unit.isdead);
			vec_pushback(game.enemies, testsoldier);
		}

		done = 1;
	}
}

void wave3(void)
{
	static int done = 0;
	if(!done)
	{
		texture_ctortext(&game.wave, &game.font, "Wave 3/4", &game.window);
		game.wavenum = 3;

		struct SoldierUnit testsoldier;
		soldierunit_ctor(
			&testsoldier, 
			(struct Vec2d){
				.x = MAP_WIDTH / 2.0, 
				.y = 0
			}, 
			&game
		);
		testsoldier.isenemy = 1;
		testsoldier.command = SOLDIERCOMMAND_OFFENSIVE;
		vec_pushback(game.enemies, testsoldier);

		soldierunit_ctor(
			&testsoldier, 
			(struct Vec2d){
				.x = MAP_WIDTH / 2.0, 
				.y = 0
			}, 
			&game
		);
		testsoldier.isenemy = 1;
		testsoldier.command = SOLDIERCOMMAND_OFFENSIVE;
		vec_pushback(game.enemies, testsoldier);

		soldierunit_ctor(
			&testsoldier, 
			(struct Vec2d){
				.x = MAP_WIDTH / 2.0, 
				.y = MAP_HEIGHT
			}, 
			&game
		);
		testsoldier.isenemy = 1;
		testsoldier.command = SOLDIERCOMMAND_OFFENSIVE;
		vec_pushback(game.enemies, testsoldier);

		soldierunit_ctor(
			&testsoldier, 
			(struct Vec2d){
				.x = 0,
				.y = MAP_HEIGHT / 2.0
			}, 
			&game
		);
		testsoldier.isenemy = 1;
		testsoldier.command = SOLDIERCOMMAND_OFFENSIVE;
		vec_pushback(game.enemies, testsoldier);

		soldierunit_ctor(
			&testsoldier, 
			(struct Vec2d){
				.x = MAP_WIDTH,
				.y = MAP_HEIGHT / 2.0
			}, 
			&game
		);
		testsoldier.isenemy = 1;
		testsoldier.command = SOLDIERCOMMAND_OFFENSIVE;
		vec_pushback(game.enemies, testsoldier);

		done = 1;
	}
}

void wave2(void)
{
	static int done = 0;
	if(!done)
	{
		texture_ctortext(&game.wave, &game.font, "Wave 2/4", &game.window);
		game.wavenum = 2;
		for(size_t i = 0; i < 3; i++)
		{
			struct SoldierUnit testsoldier;
			soldierunit_ctor(
				&testsoldier, 
				(struct Vec2d){
					.x = MAP_WIDTH - 100, 
					.y = MAP_HEIGHT / 2.0 - 500 + i * 500
				}, 
				&game
			);
			testsoldier.isenemy = 1;
			testsoldier.command = SOLDIERCOMMAND_OFFENSIVE;
			vec_pushback(game.enemies, testsoldier);
		}

		done = 1;
	}
}

void wave1(void)
{
	static int done = 0;
	if(!done)
	{
		log_info("Wave 1");
		game.wavenum = 1;
		struct SoldierUnit testsoldier;
		soldierunit_ctor(
			&testsoldier, 
			(struct Vec2d){.x = MAP_WIDTH / 3, .y = MAP_HEIGHT / 3}, 
			&game
		);
		testsoldier.isenemy = 1;
		testsoldier.command = SOLDIERCOMMAND_OFFENSIVE;
		vec_pushback(game.enemies, testsoldier);
		done = 1;
	}
}

void game_reset(struct Game* self, struct GameLoop* loop)
{
	log_assert(self, "is NULL");
	
	window_grabmouse(&self->window, 1);
	vec_clear(loop->timedcallbacks);
	player_dtor(&self->player);
	player_ctor(
		&self->player, 
		(struct Vec2d){
			.x = MAP_WIDTH  / 2.0,
			.y = MAP_HEIGHT / 2.0,
		}, 
		self, 
		loop
	);
	camera_ctor(
		&self->camera,
		(struct Vec2d){
			.x = MAP_WIDTH  / 2.0,
			.y = MAP_HEIGHT / 2.0,
		}
	);
	self->paused = 0;
}

void update(struct GameLoop* loop, void* userdata)
{
	(void)loop;
	(void)userdata;

	inputhandler_update(&game.input);
	switch(game.state)
	{
	case GAMESTATE_MENU:
		for(size_t i = 0; i < vec_getsize(game.input.events); i++)
		{
			switch(game.input.events[i].type)
			{
			case EVENTTYPE_QUIT:
				loop->done = 1;
				break;

			default:
				break;
			}
		}

		mainmenu_update(&game.mainmenu, &game, loop);
		break;

	case GAMESTATE_WIN:
		game.gameovermenu.win = 1;
		gameovermenu_update(&game.gameovermenu, &game, loop);
		break;

	case GAMESTATE_LOSE:
		game.gameovermenu.win = 0;
		gameovermenu_update(&game.gameovermenu, &game, loop);
		break;

	case GAMESTATE_NORMAL:
		if(loop->seconds - starttime >= 30)
		{
			wave1();
		}
		if(loop->seconds - starttime >= 50)
		{
			wave2();
		}
		if(loop->seconds - starttime >= 80)
		{
			wave3();
		}
		if(loop->seconds - starttime >= 110)
		{
			wave4();
		}

		if(game.wavenum == 4)
		{
			log_info("Wave is 4");
			int someonealive = 0;
			for(size_t i = 0; i < vec_getsize(game.enemies); i++)
			{
				if(!game.enemies[i].unit.isdead)
				{
					someonealive = 1;
					break;
				}
			}

			if(!someonealive)
			{
				log_info("Noone is alive");
				game.state = GAMESTATE_WIN;
			}
			else
			{
				log_info("Someone is alive");
			}
		}

		for(size_t i = 0; i < vec_getsize(game.input.events); i++)
		{
			switch(game.input.events[i].type)
			{
			case EVENTTYPE_QUIT:
				loop->done = 1;
				break;

			case EVENTTYPE_KEYDOWN:
				if(game.input.events[i].key == SDL_SCANCODE_ESCAPE
					|| game.input.events[i].key == SDL_SCANCODE_P)
				{
					game.paused = !game.paused;
					window_grabmouse(&game.window, !game.paused);
				}
				break;

			case EVENTTYPE_BUTTONDOWN:
				if(game.input.events[i].cbutton == CONTROLLERBUTTON_START)
				{
					game.paused = !game.paused;
					window_grabmouse(&game.window, !game.paused);
				}
				break;

			default:
				break;
			}
		}

		if(game.paused)
		{
			pausemenu_update(&game.pausemenu, &game, loop);
		}
		else
		{
			for(size_t i = 0; i < vec_getsize(game.enemies); i++)
			{
				soldierunit_update(&game.enemies[i], &game, loop);
			}

			for(size_t i = 0; i < vec_getsize(game.projectiles); i++)
			{
				projectile_update(&game.projectiles[i], &game, loop);
			}

			player_update(&game.player, &game, loop);
			camera_update(&game.camera, &game, loop);
		}
		break;
	}
}

void render(struct GameLoop* loop, void* userdata)
{
	(void)loop;
	(void)userdata;

	window_setdrawcolor(&game.window, color(0, 0, 0, 255));
	window_clear(&game.window);
	switch(game.state)
	{
	case GAMESTATE_MENU:
		mainmenu_render(&game.mainmenu, &game, loop);
		break;

	case GAMESTATE_WIN:
	case GAMESTATE_LOSE:
		gameovermenu_render(&game.gameovermenu, &game, loop);
		break;

	case GAMESTATE_NORMAL:
		if(game.paused)
		{
			pausemenu_render(&game.pausemenu, &game, loop);
		}
		else
		{
			camera_render(&game.camera, &game, loop);
			tilemap_render(&game.tilemap, &game);
			for(size_t i = 0; i < vec_getsize(game.enemies); i++)
			{
				soldierunit_render(&game.enemies[i], &game, loop);
			}

			for(size_t i = 0; i < vec_getsize(game.projectiles); i++)
			{
				projectile_render(&game.projectiles[i], &game, loop);
			}

			player_render(&game.player, &game, loop);

			window_setdrawcolor(&game.window, color(0, 0, 0, 170));
			window_renderfillrect(
				&game.window, 
				&(struct Rect){
					.pos = {
						.x = game.window.width - game.wave.width - 40,
						.y = 0
					},
					.width = game.wave.width + 40,
					.height = 60
				}
			);
			window_rendertexture(
				&game.window,
				&game.wave,
				NULL,
				&(struct Rect){
					.pos = {
						.x = game.window.width - game.wave.width - 20,
						.y = 5,
					},
					.width = game.wave.width,
					.height = game.wave.height
				}
			);
		}
		break;
	}

	window_present(&game.window);
}

void onerror(void* udata)
{
	(void)udata;
	log_warning("Error occured, trying to continue. ");
	//abort();
}

int main(void)
{
	log_seterrorhandler(onerror, NULL);
	initialize();

	struct Settings settings = setup();
	int flags = WINDOW_DEFAULT;
	if(settings.vsync)
	{
		flags |= WINDOW_VSYNC;
	}

	if(settings.fullscreen)
	{
		flags |= WINDOW_FULLSCREENDESKTOP;
	}

	game.controller = settings.controller;
	struct GameLoop loop;
	gameloop_ctor(
		&loop, 
		TICKSPERSEC, 
		(struct GameLoopCallback){
			.func     = update,
			.userdata = NULL
		}, 
		(struct GameLoopCallback){
			.func     = render,
			.userdata = NULL
		}
	);

	window_ctor(
		&game.window, 
		"¡Hola!", 
		settings.display, 
		settings.driver, 
		XRESOLUTION, 
		YRESOLUTION, 
		flags
	);
	game.projectiles = vec_new(struct Projectile, 1000);
	game.enemies = vec_new(struct SoldierUnit, 150);
	tilemap_ctor(&game.tilemap, &game);
	inputhandler_ctor(&game.input);
	texture_ctorimage(&game.circle, "res/circle.png", &game.window);
	texture_ctorimage(&game.laser, "res/laser.png", &game.window);
	font_ctor(
		&game.font, 
		"./res/space_age.ttf", 
		48, 
		color(180, 210, 255, 255), 
		&game.window
	);
	texture_ctortext(&game.offensive, &game.font, "Offensive",  &game.window);
	texture_ctortext(&game.follow,    &game.font, "Follow",     &game.window);
	texture_ctortext(&game.shoot,     &game.font, "Shoot",      &game.window);
	texture_ctortext(&game.wave, &game.font, "Wave 1/4", &game.window);

	game.wavenum = 1;
	player_ctor(
		&game.player, 
		(struct Vec2d){
			.x = MAP_WIDTH  / 2.0,
			.y = MAP_HEIGHT / 2.0,
		}, 
		&game, 
		&loop
	);
	camera_ctor(
		&game.camera, 
		(struct Vec2d){
			.x = MAP_WIDTH  / 2.0,
			.y = MAP_HEIGHT / 2.0,
		}
	);
	mainmenu_ctor(&game.mainmenu, &game);
	pausemenu_ctor(&game.pausemenu, &game);
	gameovermenu_ctor(&game.gameovermenu, &game);
	game.state = GAMESTATE_MENU;

	gameloop_start(&loop);

	//Free stuff
	gameovermenu_dtor(&game.gameovermenu);
	pausemenu_dtor(&game.pausemenu);
	mainmenu_dtor(&game.mainmenu);
	player_dtor(&game.player);
	texture_dtor(&game.offensive);
	texture_dtor(&game.follow);
	texture_dtor(&game.shoot);    
	font_dtor(&game.font);
	texture_dtor(&game.laser);
	texture_dtor(&game.circle);
	tilemap_dtor(&game.tilemap);
	inputhandler_dtor(&game.input);
	window_dtor(&game.window);
	gameloop_dtor(&loop);
	cleanup();
}

