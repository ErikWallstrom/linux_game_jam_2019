#ifndef SETUP_H
#define SETUP_H

struct Settings
{
	int display;
	int driver;
	int controller;
	int fullscreen;
	int vsync;
};

struct Settings setup(void);

#endif
