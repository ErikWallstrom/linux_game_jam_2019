#ifndef GAMEOVERMENU_H
#define GAMEOVERMENU_H

#include "button.h"

struct Game;
struct GameLoop;

struct GameOverMenu
{
	struct Texture losetext;
	struct Rect loserect;
	struct Texture wintext;
	struct Rect winrect;
	struct Button mainmenu;
	int win;
};

struct GameOverMenu* gameovermenu_ctor(
	struct GameOverMenu* self, 
	struct Game* game
);
void gameovermenu_update(
	struct GameOverMenu* self, 
	struct Game* game, 
	struct GameLoop* loop
);
void gameovermenu_render(
	struct GameOverMenu* self, 
	struct Game* game, 
	struct GameLoop* loop
);
void gameovermenu_dtor(struct GameOverMenu* self);

#endif
