#include "gameovermenu.h"
#include "game.h"
#include "../../gameloop.h"
#include "../../log.h"

struct GameOverMenu* gameovermenu_ctor(
	struct GameOverMenu* self, 
	struct Game* game)
{
	log_assert(self, "is NULL");
	log_assert(game, "is NULL");

	texture_ctortext(
		&self->wintext,
		&game->font, 
		"You won!", 
		&game->window
	);
	texture_ctortext(
		&self->losetext,
		&game->font, 
		"You died!", 
		&game->window
	);
	texture_ctortext(
		&self->mainmenu.texture,
		&game->font, 
		"Main menu", 
		&game->window
	);
	rect_ctor(
		&self->mainmenu.rect, 
		(struct Vec2d){
			.x = game->window.width  / 2.0, 
			.y = game->window.height / 2.0
		},
		RECTREGPOINT_CENTER,
		self->mainmenu.texture.width,
		self->mainmenu.texture.height
	);
	rect_ctor(
		&self->loserect,
		(struct Vec2d){
			.x = game->window.width  / 2.0, 
			.y = game->window.height / 2.0 - 100
		},
		RECTREGPOINT_CENTER,
		self->losetext.width,
		self->losetext.height
	);
	rect_ctor(
		&self->winrect,
		(struct Vec2d){
			.x = game->window.width  / 2.0, 
			.y = game->window.height / 2.0 - 100
		},
		RECTREGPOINT_CENTER,
		self->wintext.width,
		self->wintext.height
	);

	self->mainmenu.selected = 0;
	self->mainmenu.pressed = 0;
	
	return self;
}

void gameovermenu_update(
	struct GameOverMenu* self, 
	struct Game* game, 
	struct GameLoop* loop)
{
	log_assert(self, "is NULL");
	log_assert(game, "is NULL");
	log_assert(loop, "is NULL");

	for(size_t i = 0; i < vec_getsize(game->input.events); i++)
	{
		switch(game->input.events[i].type)
		{
		case EVENTTYPE_AXISMOTION:
		case EVENTTYPE_BUTTONDOWN:
			self->mainmenu.usingcontroller = 1;
			self->mainmenu.selected = 1;
			break;

		case EVENTTYPE_MOUSEMOTION:
		case EVENTTYPE_MOUSEDOWN:
			self->mainmenu.usingcontroller = 0;
			break;

		default:
			break;
		}
	}
	
	button_update(&self->mainmenu, game);
	if(self->mainmenu.pressed)
	{
		self->mainmenu.pressed = 0;
		game->state = GAMESTATE_MENU;
		loop->done = 1;
	}
}

void gameovermenu_render(
	struct GameOverMenu* self, 
	struct Game* game, 
	struct GameLoop* loop)
{
	log_assert(self, "is NULL");
	log_assert(game, "is NULL");
	log_assert(loop, "is NULL");

	if(self->win)
	{
		window_rendertexture(
			&game->window,
			&self->wintext,
			NULL,
			&self->winrect
		);
	}
	else
	{
		window_rendertexture(
			&game->window,
			&self->losetext,
			NULL,
			&self->loserect
		);
	}

	button_render(&self->mainmenu, game);
}

void gameovermenu_dtor(struct GameOverMenu* self)
{
	log_assert(self, "is NULL");
	
	texture_dtor(&self->losetext);
	texture_dtor(&self->wintext);
	texture_dtor(&self->mainmenu.texture);
}

