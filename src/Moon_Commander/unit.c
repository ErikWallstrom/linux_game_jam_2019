#include "unit.h"
#include "game.h"
#include "tilemap.h"
#include "../../gameloop.h"
#include "../../log.h"

void unit_update(struct Unit* self, double tickrate)
{
	log_assert(self, "is NULL");

	if(!self->isdead)
	{
		vec2d_normalize(&self->vel, &self->vel);
		vec2d_scale(&self->vel, self->speed * (tickrate / 1000.0), &self->vel);
		self->oldpos = self->rect.pos;
		vec2d_add(&self->rect.pos, &self->vel, &self->rect.pos);
		self->vel = (struct Vec2d){0};

		if(self->rect.pos.x < 0)
		{
			self->rect.pos.x = 0;
		}
		else if(self->rect.pos.x + self->rect.width > MAP_WIDTH)
		{
			self->rect.pos.x = MAP_WIDTH - self->rect.width;
		}

		if(self->rect.pos.y < 0)
		{
			self->rect.pos.y = 0;
		}
		else if(self->rect.pos.y + self->rect.height > MAP_HEIGHT)
		{
			self->rect.pos.y = MAP_HEIGHT - self->rect.height;
		}

		if(self->hp <= 0)
		{
			self->hp = 0;
			self->isdead = 1;
		}
	}
}

void unit_render(
	struct Unit* self, 
	struct Game* game, 
	struct GameLoop* loop)
{
	log_assert(self, "is NULL");
	log_assert(game, "is NULL");
	log_assert(loop, "is NULL");
	
	if(!self->isdead)
	{
		self->renderpos = (struct Vec2d){
			.x = self->oldpos.x + (self->rect.pos.x - self->oldpos.x) 
				* loop->interpolation - game->camera.renderpos.x,
			.y = self->oldpos.y + (self->rect.pos.y - self->oldpos.y) 
				* loop->interpolation - game->camera.renderpos.y
		};

		if(self->texture.raw)
		{
			window_rendertexture(
				&game->window, 
				&self->texture,
				NULL,
				&(struct Rect){
					.pos = self->renderpos,
					.width  = self->rect.width,
					.height = self->rect.height,
				}
			);
		}
		else
		{
			window_renderfillrect(
				&game->window, 
				&(struct Rect){
					.pos = self->renderpos,
					.width  = self->rect.width,
					.height = self->rect.height,
				}
			);
		}

		struct Rect rect;
		rect_ctor(
			&rect, 
			(struct Vec2d){
				.x = self->renderpos.x + self->rect.width  / 2.0,
				.y = self->renderpos.y + self->rect.height + 10
			}, 
			RECTREGPOINT_CENTER, 
			self->rect.width * 1.2, 
			5
		);
		window_setdrawcolor(&game->window, color(50, 50, 50, 255));
		window_renderfillrect(&game->window, &rect);

		struct Rect hprect = rect;
		hprect.width = rect.width * ((double)self->hp / self->maxhp);
		window_setdrawcolor(&game->window, color(255, 0, 0, 255));
		window_renderfillrect(&game->window, &hprect);

		window_setdrawcolor(&game->window, color(0, 0, 0, 255));
		window_renderrect(&game->window, &rect);
	}
}

