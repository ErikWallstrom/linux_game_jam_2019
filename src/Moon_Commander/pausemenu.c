#include "pausemenu.h"
#include "game.h"
#include "../../log.h"

struct PauseMenu* pausemenu_ctor(struct PauseMenu* self, struct Game* game)
{
	log_assert(self, "is NULL");
	log_assert(game, "is NULL");
	
	texture_ctortext(
		&self->resume.texture, 
		&game->font, 
		"Resume", 
		&game->window
	);
	texture_ctortext(
		&self->mainmenu.texture, 
		&game->font, 
		"Main menu", 
		&game->window
	);
	rect_ctor(
		&self->rect, 
		(struct Vec2d){
			.x = game->window.width  / 2.0, 
			.y = game->window.height / 2.0
		},
		RECTREGPOINT_CENTER,
		600,
		250
	);
	rect_ctor(
		&self->resume.rect, 
		(struct Vec2d){
			.x = game->window.width  / 2.0, 
			.y = game->window.height / 2.0 - 50
		},
		RECTREGPOINT_CENTER,
		self->resume.texture.width,
		self->resume.texture.height
	);
	rect_ctor(
		&self->mainmenu.rect, 
		(struct Vec2d){
			.x = game->window.width  / 2.0, 
			.y = game->window.height / 2.0 + 50
		},
		RECTREGPOINT_CENTER,
		self->mainmenu.texture.width,
		self->mainmenu.texture.height
	);

	self->mainmenu.selected = 0;
	self->mainmenu.pressed = 0;
	self->resume.selected = 0;
	self->resume.pressed = 0;
	self->selectedbutton = 0;

	return self;
}

void pausemenu_update(
	struct PauseMenu* self, 
	struct Game* game, 
	struct GameLoop* loop)
{
	log_assert(self, "is NULL");
	log_assert(game, "is NULL");
	log_assert(loop, "is NULL");

	for(size_t i = 0; i < vec_getsize(game->input.events); i++)
	{
		switch(game->input.events[i].type)
		{
		case EVENTTYPE_AXISMOTION:
			if(game->input.events[i].axismotion.axis == CONTROLLERAXIS_LEFTY)
			{
				if(game->input.events[i].axismotion.value == CONTROLLERAXIS_MAX)
				{
					self->selectedbutton--;
					if(self->selectedbutton < 0)
					{
						self->selectedbutton = 1;
					}
				}
				else if(game->input.events[i].axismotion.value 
					== CONTROLLERAXIS_MIN)
				{
					self->selectedbutton++;
					if(self->selectedbutton > 1)
					{
						self->selectedbutton = 0;
					}
				}

				if(self->selectedbutton == 1)
				{
					self->mainmenu.selected = 1;
					self->resume.selected = 0;
				}
				else
				{
					self->mainmenu.selected = 0;
					self->resume.selected = 1;
				}
			}
			//Fallthrough

		case EVENTTYPE_BUTTONDOWN:
			self->mainmenu.usingcontroller = 1;
			self->resume.usingcontroller = 1;
			break;

		case EVENTTYPE_MOUSEMOTION:
		case EVENTTYPE_MOUSEDOWN:
			self->mainmenu.usingcontroller = 0;
			self->resume.usingcontroller = 0;
			break;

		default:
			break;
		}
	}

	button_update(&self->resume, game);
	button_update(&self->mainmenu, game);

	if(self->resume.pressed)
	{
		self->resume.pressed = 0;
		game->paused = !game->paused;
		window_grabmouse(&game->window, !game->paused);
	}
	else if(self->mainmenu.pressed)
	{
		self->mainmenu.pressed = 0;
		game->state = GAMESTATE_MENU;
	}
}

void pausemenu_render(
	struct PauseMenu* self, 
	struct Game* game, 
	struct GameLoop* loop)
{
	log_assert(self, "is NULL");
	log_assert(game, "is NULL");
	log_assert(loop, "is NULL");
	
	window_setdrawcolor(&game->window, color(0, 0, 0, 150));
	window_renderfillrect(&game->window, &self->rect);
	
	button_render(&self->resume, game);
	button_render(&self->mainmenu, game);
}

void pausemenu_dtor(struct PauseMenu* self)
{
	log_assert(self, "is NULL");

	texture_dtor(&self->resume.texture);
	texture_dtor(&self->mainmenu.texture);
}

