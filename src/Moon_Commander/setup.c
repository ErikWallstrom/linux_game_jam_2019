#include "setup.h"
#include "../../file.h"
#include "../../log.h"
#include <SDL2/SDL.h>
#include <lauxlib.h>

#define CONFIG_FILE_PATH "config.lua"

static void clearinputbuffer(void)
{
	int c; //Clear input buffer
	while ((c = getchar()) != '\n' && c != EOF);
}

struct Settings setup(void)
{
	struct Settings settings;
	if(file_exists(CONFIG_FILE_PATH))
	{
		puts("\nLoading settings from config file\n");
		lua_State* l = luaL_newstate();
		if(luaL_dofile(l, CONFIG_FILE_PATH))
		{
			log_error("Config error: %s", lua_tostring(l, -1));
		}

		if(lua_getglobal(l, "display") == LUA_TNUMBER)
		{
			settings.display = lua_tointeger(l, -1);
		}
		else
		{
			log_error("'display' not found/valid in config file");
		}

		if(lua_getglobal(l, "driver") == LUA_TNUMBER)
		{
			settings.driver = lua_tointeger(l, -1);
		}
		else
		{
			log_error("'driver' not found/valid in config file");
		}

		if(lua_getglobal(l, "controller") == LUA_TNUMBER)
		{
			settings.controller = lua_tointeger(l, -1);
		}
		else
		{
			log_error("'controller' not found/valid in config file");
		}

		if(lua_getglobal(l, "fullscreen") == LUA_TNUMBER)
		{
			settings.fullscreen = lua_tointeger(l, -1);
		}
		else
		{
			log_error("'fullscreen' not found/valid in config file");
		}

		if(lua_getglobal(l, "vsync") == LUA_TNUMBER)
		{
			settings.vsync = lua_tointeger(l, -1);
		}
		else
		{
			log_error("'vsync' not found/valid in config file");
		}

		lua_close(l);
	}
	else
	{
		puts("\nFirst time setup! (settings will be stored in 'config.lua'): ");
		int numdisplays = SDL_GetNumVideoDisplays();
		if(numdisplays < 1)
		{
			log_error("\nNo displays found: %s", SDL_GetError());
		}
		else if(numdisplays == 1)
		{
			settings.display = 0;
			puts("\nOne display found");
		}
		else
		{
			printf(
				"\nMultiple displays found. \n"
				"Please choose your preferred monitor (0-%i, where 0 is default"
					"):\n\n", 
				numdisplays - 1
			);
			
			for(int i = 0; i < numdisplays; i++)
			{
				printf("    %i) %s\n", i, SDL_GetDisplayName(i));
			}

			printf("\n> ");
			if(scanf("%i", &settings.display) != 1 
				|| settings.display < 0 
				|| settings.display >= numdisplays)
			{
				settings.display = 0;
				puts("\nChoosing default (0)");
			}

			clearinputbuffer();
		}

		int numdrivers = SDL_GetNumRenderDrivers();
		if(numdrivers < 1)
		{
			log_error("\nNo display drivers found: %s", SDL_GetError());
		}
		else if(numdrivers == 1)
		{
			settings.display = 0;
			puts("\nOne display driver found");
		}
		else
		{
			printf(
				"\nMultiple display drivers found.\n"
				"Please choose your preferred display driver (0-%i, where 0 is"
					" default):\n\n", 
				numdrivers - 1
			);
			
			for(int i = 0; i < numdrivers; i++)
			{
				SDL_RendererInfo info;
				if(SDL_GetRenderDriverInfo(i, &info))
				{
					log_error(
						"Display driver retrieval failed %s", 
						SDL_GetError()
					);
				}

				printf("    %i) %s\n", i, info.name);
			}

			printf("\n> ");
			if(scanf("%i", &settings.driver) != 1 
				|| settings.driver < 0 
				|| settings.driver >= numdrivers)
			{
				settings.driver = 0;
				puts("\nChoosing default (0)");
			}

			clearinputbuffer();
		}
		
		int numjoysticks = SDL_NumJoysticks();
		int numcontrollers = 0;
		for(int i = 0; i < numjoysticks; i++)
		{
			if(SDL_IsGameController(i))
			{
				numcontrollers++;
			}
		}

		if(numcontrollers < 1)
		{
			settings.controller = -1;
			puts("\nNo game controllers found");
		}
		else if(numcontrollers == 1)
		{
			settings.controller = 0;
			puts("\nOne game controller found");
		}
		else
		{
			printf(
				"\nMultiple game controllers found.\n"
				"Please choose your preffered game controller (0-%i, where 0 is"
					" default):\n\n", 
				numdrivers - 1
			);
			int count = 0;
			for(int i = 0; i < numjoysticks; i++)
			{
				if(SDL_IsGameController(i))
				{
					printf(
						"    %i) %s\n", 
						count, 
						SDL_GameControllerNameForIndex(i)
					);
					count++;
				}
			}

			printf("\n> ");
			if(scanf("%i", &settings.controller) != 1 
				|| settings.controller < 0 
				|| settings.controller >= count)
			{
				settings.controller = 0;
				puts("\nChoosing default (0)");
			}

			clearinputbuffer();
		}

		puts("\nEnable fullscreen? (y/n):\n");
		while(1)
		{
			printf("> ");
			int c = getchar();
			if(c == 'y')
			{
				settings.fullscreen = 1;
				break;
			}
			else if(c == 'n')
			{
				settings.fullscreen = 0;
				break;
			}
		}

		clearinputbuffer();
		puts("\nEnable vsync? (y/n):\n");
		while(1)
		{
			printf("> ");
			int c = getchar();
			if(c == 'y')
			{
				settings.vsync = 1;
				break;
			}
			else if(c == 'n')
			{
				settings.vsync = 0;
				break;
			}
		}

		clearinputbuffer();
		struct File config;
		file_ctor(&config, strslice_literal(CONFIG_FILE_PATH), FILEMODE_WRITE);
		str_appendfmt(
			&config.content, 
			"display = %i\ndriver = %i\ncontroller = %i\nfullscreen = %i\nvsync"
				" = %i\n",
			settings.display,
			settings.driver,
			settings.controller,
			settings.fullscreen,
			settings.vsync
		);

		file_dtor(&config);
	}

	return settings;
}

