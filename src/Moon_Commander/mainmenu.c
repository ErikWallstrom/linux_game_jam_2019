#include "mainmenu.h"
#include "game.h"
#include "../../log.h"
#include "../../gameloop.h"
#include <SDL2/SDL_mixer.h>

extern int starttime;

struct MainMenu* mainmenu_ctor(struct MainMenu* self, struct Game* game)
{
	log_assert(self, "is NULL");
	log_assert(game, "is NULL");

	texture_ctortext(
		&self->title,
		&game->font, 
		"Moon Commander", 
		&game->window
	);
	texture_ctortext(
		&self->journalist.texture, 
		&game->font, 
		"Game journalist mode", 
		&game->window
	);
	texture_ctortext(
		&self->normal.texture, 
		&game->font, 
		"Normal mode", 
		&game->window
	);
	texture_ctortext(
		&self->impossible.texture, 
		&game->font, 
		"Impossible mode", 
		&game->window
	);
	texture_ctortext(
		&self->quit.texture, 
		&game->font, 
		"Quit", 
		&game->window
	);
	rect_ctor(
		&self->titlerect, 
		(struct Vec2d){
			.x = game->window.width  / 2.0, 
			.y = game->window.height / 2.0 - 200
		},
		RECTREGPOINT_CENTER,
		self->title.width * 2,
		self->title.height * 2
	);
	rect_ctor(
		&self->journalist.rect, 
		(struct Vec2d){
			.x = game->window.width  / 2.0, 
			.y = game->window.height / 2.0 - 50
		},
		RECTREGPOINT_CENTER,
		self->journalist.texture.width,
		self->journalist.texture.height
	);
	rect_ctor(
		&self->normal.rect, 
		(struct Vec2d){
			.x = game->window.width  / 2.0, 
			.y = game->window.height / 2.0 - 0
		},
		RECTREGPOINT_CENTER,
		self->normal.texture.width,
		self->normal.texture.height
	);
	rect_ctor(
		&self->impossible.rect, 
		(struct Vec2d){
			.x = game->window.width  / 2.0, 
			.y = game->window.height / 2.0 + 50
		},
		RECTREGPOINT_CENTER,
		self->impossible.texture.width,
		self->impossible.texture.height
	);
	rect_ctor(
		&self->quit.rect, 
		(struct Vec2d){
			.x = game->window.width  / 2.0, 
			.y = game->window.height / 2.0 + 150
		},
		RECTREGPOINT_CENTER,
		self->quit.texture.width,
		self->quit.texture.height
	);
	
	self->journalist.selected = 0;
	self->journalist.pressed = 0;
	self->normal.selected = 0;
	self->normal.pressed = 0;
	self->impossible.selected = 0;
	self->impossible.pressed = 0;
	self->quit.selected = 0;
	self->quit.pressed = 0;
	self->selectedbutton = 0;

	return self;
}

void mainmenu_update(
	struct MainMenu* self, 
	struct Game* game, 
	struct GameLoop* loop)
{
	log_assert(self, "is NULL");
	log_assert(game, "is NULL");
	log_assert(loop, "is NULL");

	for(size_t i = 0; i < vec_getsize(game->input.events); i++)
	{
		switch(game->input.events[i].type)
		{
		case EVENTTYPE_AXISMOTION:
			if(game->input.events[i].axismotion.axis == CONTROLLERAXIS_LEFTY)
			{
				if(game->input.events[i].axismotion.value == CONTROLLERAXIS_MAX)
				{
					self->selectedbutton--;
					if(self->selectedbutton < 0)
					{
						self->selectedbutton = 3;
					}
				}
				else if(game->input.events[i].axismotion.value 
					== CONTROLLERAXIS_MIN)
				{
					self->selectedbutton++;
					if(self->selectedbutton > 3)
					{
						self->selectedbutton = 0;
					}
				}

				switch(self->selectedbutton)
				{
				case 0:
					self->journalist.selected = 1;
					self->normal.selected = 0;
					self->impossible.selected = 0;
					self->quit.selected = 0;
					break;

				case 1:
					self->journalist.selected = 0;
					self->normal.selected = 1;
					self->impossible.selected = 0;
					self->quit.selected = 0;
					break;

				case 2:
					self->journalist.selected = 0;
					self->normal.selected = 0;
					self->impossible.selected = 1;
					self->quit.selected = 0;
					break;

				case 3:
					self->journalist.selected = 0;
					self->normal.selected = 0;
					self->impossible.selected = 0;
					self->quit.selected = 1;
					break;
				}
			}
			//Fallthrough

		case EVENTTYPE_BUTTONDOWN:
			self->journalist.usingcontroller = 1;
			self->normal.usingcontroller = 1;
			self->impossible.usingcontroller = 1;
			self->quit.usingcontroller = 1;
			break;

		case EVENTTYPE_MOUSEMOTION:
		case EVENTTYPE_MOUSEDOWN:
			self->journalist.usingcontroller = 0;
			self->normal.usingcontroller = 0;
			self->impossible.usingcontroller = 0;
			self->quit.usingcontroller = 0;
			break;

		default:
			break;
		}
	}

	button_update(&self->journalist, game);
	button_update(&self->normal, game);
	button_update(&self->impossible, game);
	button_update(&self->quit, game);

	if(self->quit.pressed)
	{
		self->quit.pressed = 0;
		loop->done = 1;
	}
	else if(self->normal.pressed)
	{
		self->normal.pressed = 0;
		game->state = GAMESTATE_NORMAL;
		game_reset(game, loop);
		starttime = loop->seconds;
	}
	else if(self->journalist.pressed)
	{
		self->journalist.pressed = 0;
		game->gameovermenu.win = 1;
		game->state = GAMESTATE_WIN;
	}
	else if(self->impossible.pressed)
	{
		Mix_Music* music = Mix_LoadMUS("res/dead.mp3");
		Mix_VolumeMusic(128);
		Mix_PlayMusic(music, 1);
		while(Mix_PlayingMusic());
		Mix_FreeMusic(music);

		self->impossible.pressed = 0;
		game->gameovermenu.win = 0;
		game->state = GAMESTATE_LOSE;
	}
}

void mainmenu_render(
	struct MainMenu* self, 
	struct Game* game, 
	struct GameLoop* loop)
{
	log_assert(self, "is NULL");
	log_assert(game, "is NULL");
	log_assert(loop, "is NULL");

	window_rendertexture(
		&game->window, 
		&self->title, 
		NULL, 
		&self->titlerect
	);
	
	button_render(&self->journalist, game);
	button_render(&self->normal, game);
	button_render(&self->impossible, game);
	button_render(&self->quit, game);
}

void mainmenu_dtor(struct MainMenu* self)
{
	log_assert(self, "is NULL");
	
	texture_dtor(&self->journalist.texture);
	texture_dtor(&self->normal.texture);
	texture_dtor(&self->impossible.texture);
	texture_dtor(&self->quit.texture);
}

