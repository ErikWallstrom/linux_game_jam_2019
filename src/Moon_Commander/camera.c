#include "camera.h"
#include "game.h"
#include "../../log.h"
#include "../../gameloop.h"
#include <math.h>

#define CAMERA_SPEED 1300

struct Camera* camera_ctor(struct Camera* self, struct Vec2d pos)
{
	log_assert(self, "is NULL");

	self->pos = pos;
	self->speed = CAMERA_SPEED;
	self->movetowardsgoal = 1;

	return self;
}

void camera_update(
	struct Camera* self, 
	struct Game* game, 
	struct GameLoop* loop)
{
	log_assert(self, "is NULL");
	log_assert(game, "is NULL");
	
	/*
	for(size_t i = 0; i < vec_getsize(game->input.events); i++)
	{
		if(game->input.events[i].type == EVENTTYPE_KEYDOWN)
		{
			if(game->input.events[i].key == SDL_SCANCODE_SPACE)
			{
				self->movetowardsgoal = !self->movetowardsgoal;
				self->speed = CAMERA_SPEED;
			}
		}
		else if(game->input.events[i].type == EVENTTYPE_BUTTONDOWN)
		{
			if(game->input.events[i].cbutton == CONTROLLERBUTTON_RIGHTSHOULDER)
			{
				self->movetowardsgoal = !self->movetowardsgoal;
				self->speed = CAMERA_SPEED;
			}
		}
	}
	*/

	self->vel = (struct Vec2d){0};
	/*
	if(game->input.keystate[SDL_SCANCODE_H])
	{
		self->vel.x = -1.0;
	}
	if(game->input.keystate[SDL_SCANCODE_J])
	{
		self->vel.y = 1.0;
	}
	if(game->input.keystate[SDL_SCANCODE_K])
	{
		self->vel.y = -1.0;
	}
	if(game->input.keystate[SDL_SCANCODE_L])
	{
		self->vel.x = 1.0;
	}

	if(game->input.mousex <= 0)
	{
		self->vel.x = -1.0;
	}
	if(game->input.mousey >= game->window.height - 1)
	{
		self->vel.y = 1.0;
	}
	if(game->input.mousey <= 0)
	{
		self->vel.y = -1.0;
	}
	if(game->input.mousex >= game->window.width - 1)
	{
		self->movetowardsgoal = 0;
		self->vel.x = 1.0;
	}
	
	double angle;
	if(inputhandler_cjoystickangle(
		&game->input, 
		game->controller, 
		CONTROLLERJOYSTICK_RIGHT, 
		&angle))
	{
		self->movetowardsgoal = 0;
		self->vel.x = cos(angle);
		self->vel.y = sin(angle);
	}

	if(self->vel.x != 0.0 || self->vel.y != 0.0)
	{
		self->movetowardsgoal = 0;
		self->speed = CAMERA_SPEED;
	}
	*/

	if(self->movetowardsgoal)
	{
		self->start = self->pos;
		self->goal = rect_getpos(
			&game->player.units[game->player.selectedunit]->rect, 
			RECTREGPOINT_CENTER
		);
		vec2d_sub(&self->goal, &self->pos, &self->vel);
		self->speed = vec2d_length(&self->vel) * 3.0;
		if(self->speed < 30)
		{
			self->speed = 0;
		}

		vec2d_normalize(&self->vel, &self->vel);
		vec2d_scale(
			&self->vel, 
			self->speed * (loop->tickrate / 1000.0), 
			&self->vel
		);
		self->oldpos = self->pos;
		vec2d_add(&self->pos, &self->vel, &self->pos);
	}
	else
	{
		vec2d_normalize(&self->vel, &self->vel);
		vec2d_scale(
			&self->vel, 
			self->speed * (loop->tickrate / 1000.0), 
			&self->vel
		);
		self->oldpos = self->pos;
		vec2d_add(&self->pos, &self->vel, &self->pos);
	}

	if(self->pos.x + game->window.width / 2.0 > MAP_WIDTH)
	{
		self->pos.x = MAP_WIDTH - game->window.width / 2.0;
	}
	else if(self->pos.x - game->window.width / 2.0 < 0)
	{
		self->pos.x = game->window.width / 2.0;
	}

	if(self->pos.y + game->window.height / 2.0 > MAP_HEIGHT)
	{
		self->pos.y = MAP_HEIGHT - game->window.height / 2.0;
	}
	else if(self->pos.y - game->window.height / 2.0 < 0)
	{
		self->pos.y = game->window.height / 2.0;
	}
}

void camera_render(
	struct Camera* self, 
	struct Game* game, 
	struct GameLoop* loop)
{
	log_assert(self, "is NULL");
	log_assert(game, "is NULL");
	log_assert(loop, "is NULL");
	
	self->renderpos = (struct Vec2d){
		.x = self->oldpos.x + (self->pos.x - self->oldpos.x) 
			* loop->interpolation - game->window.width / 2.0, 
		.y = self->oldpos.y + (self->pos.y - self->oldpos.y) 
			* loop->interpolation - game->window.height / 2.0, 
	};
}

