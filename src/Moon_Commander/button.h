#ifndef BUTTON_H
#define BUTTON_H

#include "../../texture.h"
#include "../../rect.h"

struct Game;

struct Button
{
	struct Texture texture;
	struct Rect rect;
	unsigned selected;
	unsigned pressed;
	int usingcontroller;
};

void button_update(struct Button* self, struct Game* game);
void button_render(struct Button* self, struct Game* game);

#endif
