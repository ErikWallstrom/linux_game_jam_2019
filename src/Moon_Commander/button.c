#include "button.h"
#include "game.h"
#include "../../log.h"
#include <SDL2/SDL.h>

void button_update(struct Button* self, struct Game* game)
{
	log_assert(self, "is NULL");
	log_assert(game, "is NULL");

	if(self->usingcontroller)
	{
		for(size_t i = 0; i < vec_getsize(game->input.events); i++)
		{
			if(game->input.events[i].type == EVENTTYPE_BUTTONDOWN)
			{
				if(game->input.events[i].cbutton == CONTROLLERBUTTON_A
					&& self->selected)
				{
					self->pressed++;
				}
			}
		}
	}
	else
	{
		if(rect_intersectspoint(
			&self->rect, 
			game->input.mousex, 
			game->input.mousey))
		{
			self->selected = 1;
		}
		else
		{
			self->selected = 0;
		}

		for(size_t i = 0; i < vec_getsize(game->input.events); i++)
		{
			if(game->input.events[i].type == EVENTTYPE_MOUSEDOWN)
			{
				if(game->input.events[i].mbutton == MOUSEBUTTON_LEFT 
					&& self->selected)
				{
					self->pressed++;
				}
			}
		}
	}
}

void button_render(struct Button* self, struct Game* game)
{
	log_assert(self, "is NULL");
	log_assert(game, "is NULL");
	
	window_rendertexture(&game->window, &self->texture, NULL, &self->rect);
	if(self->selected)
	{
		window_setdrawcolor(&game->window, color(100, 100, 255, 255));
		struct Vec2d pos = rect_getpos(&self->rect, RECTREGPOINT_CENTER);
		struct Rect rect;
		rect_ctor(
			&rect, 
			pos, 
			RECTREGPOINT_CENTER, 
			self->rect.width + 12, 
			self->rect.height
		);
		window_renderrect(&game->window, &rect);
	}
}

