#include "soldierunit.h"
#include "game.h"
#include "../../gameloop.h"
#include "../../log.h"

#define SOLDIER_HP     35
#define SOLDIER_WIDTH  50
#define SOLDIER_HEIGHT 50
#define SOLDIER_SPEED  350

struct SoldierUnit* soldierunit_ctor(
	struct SoldierUnit* self, 
	struct Vec2d pos,
	struct Game* game)
{
	log_assert(self, "is NULL");
	log_assert(game, "is NULL");

	self->unit.isdead = 0;
	self->shootbutton.usingcontroller = 1;
	self->shootbutton.pressed = 0;
	self->shootbutton.selected = 0;
	self->shootbutton.texture = game->shoot;
	self->shootbutton.rect = (struct Rect){
		.pos = {
			.x = 20,
			.y = game->window.height - 55
		},
		.width = self->shootbutton.texture.width,
		.height = self->shootbutton.texture.height,
	};

	self->offensivebutton.usingcontroller = 1;
	self->offensivebutton.pressed = 0;
	self->offensivebutton.selected = 0;
	self->offensivebutton.texture = game->offensive;
	self->offensivebutton.rect = (struct Rect){
		.pos = {
			.x = self->shootbutton.rect.width + 50,
			.y = game->window.height - 55
		},
		.width = self->offensivebutton.texture.width,
		.height = self->offensivebutton.texture.height,
	};

	self->followbutton.usingcontroller = 1;
	self->followbutton.pressed = 0;
	self->followbutton.selected = 0;
	self->followbutton.texture = game->follow;
	self->followbutton.rect = (struct Rect){
		.pos = {
			.x = self->offensivebutton.rect.width + self->offensivebutton.rect
				.pos.x + 50,
			.y = game->window.height - 55
		},
		.width = self->followbutton.texture.width,
		.height = self->followbutton.texture.height,
	};

	self->command = SOLDIERCOMMAND_SHOOT;
	self->shootangle = 0.0;
	self->movetarget = NULL;
	self->unit.selected = 0;
	self->unit.vel = (struct Vec2d){0};
	self->unit.speed = SOLDIER_SPEED;
	self->unit.hp = SOLDIER_HP;
	self->unit.maxhp = SOLDIER_HP;
	self->unit.texture.raw = NULL;
	rect_ctor(
		&self->unit.rect, 
		pos,
		RECTREGPOINT_CENTER, 
		SOLDIER_WIDTH, 
		SOLDIER_HEIGHT
	);

	return self;
}

void soldierunit_shoot(struct SoldierUnit* self, struct Game* game)
{
	log_assert(self, "is NULL");
	log_assert(game, "is NULL");
	
	struct Projectile projectile = {0};
	projectile.rotation = self->shootangle;
	projectile.type = PROJECTILETYPE_LASER;
	projectile.pos = rect_getpos(&self->unit.rect, RECTREGPOINT_CENTER);
	projectile.start = projectile.pos;
	projectile.oldpos = projectile.pos;
	projectile.done = 0;
	if(self->isenemy)
	{
		projectile.allied = 0;
	}
	else
	{
		projectile.allied = 1;
	}
	vec_pushback(game->projectiles, projectile);
}

void soldierunit_update(
	struct SoldierUnit* self, 
	struct Game* game, 
	struct GameLoop* loop)
{
	log_assert(self, "is NULL");
	log_assert(game, "is NULL");
	log_assert(loop, "is NULL");

	if(!self->unit.isdead)
	{
		switch(self->command)
		{
		case SOLDIERCOMMAND_OFFENSIVE:
			self->offensivebutton.selected = 1;
			self->followbutton.selected = 0;
			self->shootbutton.selected = 0;
			break;

		case SOLDIERCOMMAND_SHOOT:
			self->offensivebutton.selected = 0;
			self->followbutton.selected = 0;
			self->shootbutton.selected = 1;
			break;

		case SOLDIERCOMMAND_FOLLOW:
			self->offensivebutton.selected = 0;
			self->followbutton.selected = 1;
			self->shootbutton.selected = 0;
			break;
		}

		if(!self->unit.selected)
		{
			switch(self->command)
			{
			case SOLDIERCOMMAND_OFFENSIVE:
				if(self->isenemy)
				{
					self->unit.speed = SOLDIER_SPEED - 100;
					if(vec_getsize(game->player.targetunits))
					{
						int i = (int)(vec_getsize(game->player.targetunits) - 1);
						while(!self->movetarget || self->movetarget->isdead)
						{
							self->movetarget = game->player.targetunits[i];
							if(!self->movetarget->isdead)
							{
								break;
							}

							i--;
							if(i == -1)
							{
								self->movetarget = NULL;
								break;
							}
						}
					}
					else
					{
						self->movetarget = NULL;
					}
				}
				else
				{
					if(vec_getsize(game->enemies))
					{
						int i = (int)(vec_getsize(game->enemies) - 1);
						while(!self->movetarget || self->movetarget->isdead)
						{
							self->movetarget = &game->enemies[i].unit;
							if(!self->movetarget->isdead)
							{
								break;
							}

							i--;
							if(i == -1)
							{
								self->movetarget = NULL;
								break;
							}
						}
					}
					else
					{
						self->movetarget = NULL;
					}
				}

				if(self->movetarget)
				{
					vec2d_sub(
						&self->movetarget->rect.pos, 
						&self->unit.rect.pos, 
						&self->unit.vel
					);
					self->shootangle = atan2(self->unit.vel.y, self->unit.vel.x);
					if(vec2d_length(&self->unit.vel) <= 400)
					{
						self->unit.vel = (struct Vec2d){0};
					}
				}

				//Fallthrough

			case SOLDIERCOMMAND_SHOOT:
				if(loop->tickscount % 20 == 0)
				{
					soldierunit_shoot(self, game);
				}
				break;

			case SOLDIERCOMMAND_FOLLOW:
				self->unit.speed = game->player.player.unit.speed;
				self->unit.vel = game->player.player.unit.vel;
				break;
			}
		}
		else
		{
			self->unit.speed = SOLDIER_SPEED;
			inputhandler_cjoystickangle(
				&game->input, 
				game->controller, 
				CONTROLLERJOYSTICK_RIGHT, 
				&self->shootangle
			);

			for(size_t i = 0; i < vec_getsize(game->input.events); i++)
			{
				switch(game->input.events[i].type)
				{
				case EVENTTYPE_AXISMOTION:
					if(game->input.events[i].axismotion.axis 
							== CONTROLLERAXIS_TRIGGERRIGHT 
						&& game->input.events[i].axismotion.value
							== CONTROLLERAXIS_MIN)
					{
						soldierunit_shoot(self, game);
					}
					break;

				case EVENTTYPE_MOUSEDOWN:
					if(game->input.events[i].mbutton == MOUSEBUTTON_LEFT)
					{
						double diffx = game->input.mousex + game->camera.pos.x
							- game->window.width  / 2.0 - self->unit.rect.pos.x;
						double diffy = game->input.mousey + game->camera.pos.y
							- game->window.height / 2.0 - self->unit.rect.pos.y;
						self->shootangle = atan2(diffy, diffx);

						soldierunit_shoot(self, game);
					}
					break;

				case EVENTTYPE_BUTTONDOWN:
					if(game->input.events[i].cbutton 
						== CONTROLLERBUTTON_LEFTSHOULDER)
					{
						switch(self->command)
						{
						case SOLDIERCOMMAND_SHOOT:
							self->command = SOLDIERCOMMAND_OFFENSIVE;
							break;

						case SOLDIERCOMMAND_OFFENSIVE:
							self->command = SOLDIERCOMMAND_FOLLOW;
							break;
							
						case SOLDIERCOMMAND_FOLLOW:
							self->command = SOLDIERCOMMAND_SHOOT;
							break;
						}
					}
					break;

				case EVENTTYPE_KEYDOWN:
					if(game->input.events[i].key == SDL_SCANCODE_Q)
					{
						switch(self->command)
						{
						case SOLDIERCOMMAND_SHOOT:
							self->command = SOLDIERCOMMAND_OFFENSIVE;
							break;

						case SOLDIERCOMMAND_OFFENSIVE:
							self->command = SOLDIERCOMMAND_FOLLOW;
							break;
							
						case SOLDIERCOMMAND_FOLLOW:
							self->command = SOLDIERCOMMAND_SHOOT;
							break;
						}
					}
					break;

				default:
					break;
				}
			}
		}

		for(size_t i = 0; i < vec_getsize(game->projectiles); i++)
		{
			if(self->isenemy)
			{
				if(!game->projectiles[i].done && game->projectiles[i].allied)
				{
					double x = game->projectiles[i].pos.x 
						+ cos(game->projectiles->rotation) * game->laser.width 
							/ 2.0;
					double y = game->projectiles[i].pos.y
						+ sin(game->projectiles->rotation) * game->laser.width 
							/ 2.0;

					if(rect_intersectspoint(&self->unit.rect, x, y))
					{
						self->unit.hp -= 5;
						game->projectiles[i].done = 1;
					}
				}
			}
			else
			{
				if(!game->projectiles[i].done && !game->projectiles[i].allied)
				{
					double x = game->projectiles[i].pos.x 
						+ cos(game->projectiles->rotation) * game->laser.width 
							/ 2.0;
					double y = game->projectiles[i].pos.y
						+ sin(game->projectiles->rotation) * game->laser.width 
							/ 2.0;

					if(rect_intersectspoint(&self->unit.rect, x, y))
					{
						self->unit.hp -= 5;
						game->projectiles[i].done = 1;
					}
				}
			}
		}

		unit_update(&self->unit, loop->tickrate);
	}
}

void soldierunit_render(
	struct SoldierUnit* self, 
	struct Game* game, 
	struct GameLoop* loop)
{
	log_assert(self, "is NULL");
	log_assert(game, "is NULL");
	log_assert(loop, "is NULL");

	if(!self->unit.isdead)
	{
		if(self->isenemy)
		{
			window_setdrawcolor(&game->window, color(255, 0, 0, 255));
		}
		else
		{
			window_setdrawcolor(&game->window, color(0, 255, 0, 255));
		}

		unit_render(&self->unit, game, loop);

		if(self->unit.selected)
		{
			struct Vec2d pos1 = {
				.x = self->unit.renderpos.x + self->unit.rect.width  / 2.0,
				.y = self->unit.renderpos.y + self->unit.rect.height / 2.0,
			};
			struct Vec2d pos2 = pos1; 
			struct Vec2d temp = {
				.x = cos(self->shootangle),
				.y = sin(self->shootangle)
			};
			vec2d_normalize(&temp, &temp);
			vec2d_scale(&temp, 100, &temp);
			vec2d_add(&pos2, &temp, &pos2);
			window_setdrawcolor(&game->window, color(255, 0, 0, 255));
			window_renderline(&game->window, pos1, pos2);

			window_setdrawcolor(&game->window, color(0, 0, 0, 170));
			window_renderfillrect(
				&game->window, 
				&(struct Rect){
					.pos = {
						.x = 0,
						.y = game->window.height - 60
					},
					.width = 950,
					.height = 60
				}
			);

			button_render(&self->shootbutton, game);
			button_render(&self->followbutton, game);
			button_render(&self->offensivebutton, game);
		}
	}
}

