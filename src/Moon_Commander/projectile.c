#include "projectile.h"
#include "game.h"
#include "../../log.h"
#include "../../gameloop.h"

#define LASER_SPEED 1000
#define LASER_RANGE 800

void projectile_update(
	struct Projectile* self, 
	struct Game* game,
	struct GameLoop* loop)
{
	log_assert(self, "is NULL");
	log_assert(game, "is NULL");
	log_assert(loop, "is NULL");

	if(!self->done)
	{
		double speed;
		double range;
		switch(self->type)
		{
		case PROJECTILETYPE_LASER: 
			speed = LASER_SPEED;
			range = LASER_RANGE;
			break;

		default:
			speed = 0;
			range = 123123;
			break;
		}

		struct Vec2d vel = {.x = cos(self->rotation), .y = sin(self->rotation)};
		vec2d_normalize(&vel, &vel); //?
		vec2d_scale(&vel, speed * (loop->tickrate / 1000.0), &vel);

		self->oldpos = self->pos;
		vec2d_add(&self->pos, &vel, &self->pos);

		struct Vec2d diff;
		vec2d_sub(&self->pos, &self->start, &diff);
		if(vec2d_length(&diff) >= range)
		{
			self->done = 1;
		}
	}
}

void projectile_render(
	struct Projectile* self, 
	struct Game* game, 
	struct GameLoop* loop)
{
	log_assert(self, "is NULL");
	log_assert(game, "is NULL");
	log_assert(loop, "is NULL");

	if(!self->done)
	{
		struct Texture* texture;
		double width;
		double height;
		switch(self->type)
		{
		case PROJECTILETYPE_LASER: 
			width = game->laser.width;
			height = game->laser.height;
			texture = &game->laser;
			break;

		default:
			log_assert(0, "??");
			width = 0;
			height = 0;
			texture = NULL;
			break;
		}

		struct Vec2d renderpos = {
			.x = self->oldpos.x + (self->pos.x - self->oldpos.x) 
				* loop->interpolation - game->camera.renderpos.x,
			.y = self->oldpos.y + (self->pos.y - self->oldpos.y) 
				* loop->interpolation - game->camera.renderpos.y
		};
		struct Rect rect; 
		rect_ctor(&rect, renderpos, RECTREGPOINT_CENTER, width, height);
		window_setdrawcolor(&game->window, color(255, 0, 0, 255));
		window_renderrotatedtexture(
			&game->window, 
			texture,
			NULL,
			&rect,
			self->rotation * 180 / M_PI,
			RECTREGPOINT_CENTER
		);
	}
}

