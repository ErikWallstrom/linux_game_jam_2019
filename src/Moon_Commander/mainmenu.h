#ifndef MAINMENU_H
#define MAINMENU_H

#include "button.h"

struct Game;
struct GameLoop;

struct MainMenu
{
	struct Texture title;
	struct Rect titlerect;
	struct Button journalist;
	struct Button normal;
	struct Button impossible;
	struct Button quit;
	int selectedbutton;
};

struct MainMenu* mainmenu_ctor(struct MainMenu* self, struct Game* game);
void mainmenu_update(
	struct MainMenu* self, 
	struct Game* game, 
	struct GameLoop* loop
);
void mainmenu_render(
	struct MainMenu* self, 
	struct Game* game, 
	struct GameLoop* loop
);
void mainmenu_dtor(struct MainMenu* self);

#endif
