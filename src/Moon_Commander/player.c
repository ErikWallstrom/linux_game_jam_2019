#include "game.h"
#include "../../gameloop.h"
#include "../../str.h"
#include "../../log.h"

#define PLAYER_HP  	  100
#define PLAYER_WIDTH  100
#define PLAYER_HEIGHT 100
#define PLAYER_SPEED  400

#define SOLARPANEL_HP  	  175
#define SOLARPANEL_WIDTH  150
#define SOLARPANEL_HEIGHT 150

void solarpanelincome(struct GameLoop* loop, void* udata)
{
	log_assert(loop, "is NULL");
	log_assert(udata, "is NULL");

	struct Player* player = udata;
	player->power += 1;
}

void baseincome(struct GameLoop* loop, void* udata)
{
	log_assert(loop, "is NULL");
	log_assert(udata, "is NULL");

	struct Player* player = udata;
	player->power += 1;
}

void playerunit_shoot(struct PlayerUnit* self, struct Game* game)
{
	log_assert(self, "is NULL");

	for(size_t i = 0; i < vec_getsize(game->player.soldiers); i++)
	{
		if(game->player.soldiers[i].command == SOLDIERCOMMAND_FOLLOW)
		{
			game->player.soldiers[i].shootangle = self->shootangle;
			soldierunit_shoot(&game->player.soldiers[i], game);
		}
	}

	struct Projectile projectile = {0};
	projectile.rotation = self->shootangle;
	projectile.type = PROJECTILETYPE_LASER;
	projectile.pos = rect_getpos(&self->unit.rect, RECTREGPOINT_CENTER);
	projectile.start = projectile.pos;
	projectile.oldpos = projectile.pos;
	projectile.done = 0;
	projectile.allied = 1;
	vec_pushback(game->projectiles, projectile);
}

struct Player* player_ctor(
	struct Player* self, 
	struct Vec2d pos, 
	struct Game* game,
	struct GameLoop* loop)
{
	log_assert(self, "is NULL");
	log_assert(game, "is NULL");

	gameloop_addcallback(
		loop, 
		(struct GameLoopCallback){
			baseincome,
			self 
		}, 
		1000
	);
	self->player.unit.isdead = 0;
	self->action = PLAYERACTION_BUILDSOLAR;
	self->soldiers = vec_new(struct SoldierUnit, 100);
	self->panels = vec_new(struct SolarPanel, 100);
	self->targetunits = vec_new(struct Unit*, 100);
	self->units = vec_new(struct Unit*, 100);
	self->selectedunit = 0;
	self->power = 0;
	self->powerdisplayed = 0;
	texture_ctorimage(&self->solarpanel, "res/solarpanel.png", &game->window);
	texture_ctortext(
		&self->powertext,
		&game->font,
		"Power: 0",
		&game->window
	);
	texture_ctortext(
		&self->solartext,
		&game->font,
		"Build(15)",
		&game->window
	);
	texture_ctortext(
		&self->soldiertext,
		&game->font,
		"Recruit(25)",
		&game->window
	);
	self->player.unit.selected = 1;
	self->player.unit.speed = PLAYER_SPEED;
	self->player.unit.vel = (struct Vec2d){0};
	self->player.unit.hp = PLAYER_HP;
	self->player.unit.maxhp = PLAYER_HP;
	self->player.unit.texture.raw = NULL;
	rect_ctor(
		&self->player.unit.rect, 
		pos, 
		RECTREGPOINT_CENTER, 
		PLAYER_WIDTH, 
		PLAYER_HEIGHT
	);
	vec_pushback(self->units, &self->player.unit);
	vec_pushback(self->targetunits, &self->player.unit);

	self->buildsolar.usingcontroller = 1;
	self->buildsolar.pressed = 0;
	self->buildsolar.selected = 0;
	self->buildsolar.texture = self->solartext;
	self->buildsolar.rect = (struct Rect){
		.pos = {
			.x = 20,
			.y = game->window.height - 55
		},
		.width =  self->buildsolar.texture.width,
		.height = self->buildsolar.texture.height,
	};

	self->buildsoldier.usingcontroller = 1;
	self->buildsoldier.pressed = 0;
	self->buildsoldier.selected = 0;
	self->buildsoldier.texture = self->soldiertext;
	self->buildsoldier.rect = (struct Rect){
		.pos = {
			.x = self->buildsolar.rect.width + 80,
			.y = game->window.height - 55
		},
		.width =  self->buildsoldier.texture.width,
		.height = self->buildsoldier.texture.height,
	};

	return self;
}

void player_update(
	struct Player* self, 
	struct Game* game, 
	struct GameLoop* loop)
{
	log_assert(self, "is NULL");
	log_assert(game, "is NULL");
	log_assert(loop, "is NULL");

	switch(self->action)
	{
	case PLAYERACTION_BUILDSOLAR:
		self->buildsolar.selected = 1;
		self->buildsoldier.selected = 0;
		break;

	case PLAYERACTION_BUILDSOLDIER:
		self->buildsolar.selected = 0;
		self->buildsoldier.selected = 1;
		break;
	}

	if(self->powerdisplayed != self->power)
	{
		texture_dtor(&self->powertext);

		struct Str str;
		str_ctorfmt(&str, "Power: %i", self->power);
		texture_ctortext(
			&self->powertext,
			&game->font,
			str.data,
			&game->window
		);
		str_dtor(&str);
	}

	if(game->input.keystate[SDL_SCANCODE_A])
	{
		self->units[self->selectedunit]->vel.x = -1.0;
	}
	if(game->input.keystate[SDL_SCANCODE_S])
	{
		self->units[self->selectedunit]->vel.y = 1.0;
	}
	if(game->input.keystate[SDL_SCANCODE_W])
	{
		self->units[self->selectedunit]->vel.y = -1.0;
	}
	if(game->input.keystate[SDL_SCANCODE_D])
	{
		self->units[self->selectedunit]->vel.x = 1.0;
	}

	for(size_t i = 0; i < vec_getsize(game->input.events); i++)
	{
		switch(game->input.events[i].type)
		{
		case EVENTTYPE_MOUSEDOWN:
			if(game->input.events[i].mbutton == MOUSEBUTTON_LEFT 
				&& self->player.unit.selected)
			{
				double diffx = game->input.mousex + game->camera.pos.x
					- game->window.width  / 2.0 - self->player.unit.rect.pos.x;
				double diffy = game->input.mousey + game->camera.pos.y
					- game->window.height / 2.0 - self->player.unit.rect.pos.y;
				self->player.shootangle = atan2(diffy, diffx);
				playerunit_shoot(&self->player, game);
			}
			break;

		case EVENTTYPE_AXISMOTION:
			if(game->input.events[i].axismotion.axis 
					== CONTROLLERAXIS_TRIGGERRIGHT 
				&& game->input.events[i].axismotion.value
					== CONTROLLERAXIS_MIN
				&& self->player.unit.selected)
			{
				playerunit_shoot(&self->player, game);
			}
			break;

		case EVENTTYPE_BUTTONDOWN:
			switch(game->input.events[i].cbutton)
			{
			case CONTROLLERBUTTON_LEFTSHOULDER:
				if(self->player.unit.selected)
				{
					switch(self->action)
					{
					case PLAYERACTION_BUILDSOLAR:
						self->action = PLAYERACTION_BUILDSOLDIER;
						break;

					case PLAYERACTION_BUILDSOLDIER:
						self->action = PLAYERACTION_BUILDSOLAR;
						break;
					}
				}
				break;

			case CONTROLLERBUTTON_A:
				switch(self->action)
				{
				case PLAYERACTION_BUILDSOLAR:
					{
						struct SolarPanel panel = {0};
						rect_ctor(
							&panel.unit.rect, 
							rect_getpos(
								&self->player.unit.rect, 
								RECTREGPOINT_CENTER
							), 
							RECTREGPOINT_CENTER, 
							SOLARPANEL_WIDTH,
							SOLARPANEL_HEIGHT
						);

						panel.unit.texture = self->solarpanel;
						panel.unit.hp = SOLARPANEL_HP;
						panel.unit.maxhp = SOLARPANEL_HP;
						int hit = 0;
						for(size_t j = 0;
							j < vec_getsize(self->panels); 
							j++)
						{
							if(rect_intersects(
								&panel.unit.rect, 
								&self->panels[j].unit.rect))
							{
								hit = 1;
								break;
							}
						}

						if(!hit && self->power >= 15)
						{
							self->power -= 15;
							vec_pushback(self->panels, panel);
							vec_pushback(
								self->targetunits, 
								&self->panels[vec_getsize(self->panels) - 1].unit
							);
							gameloop_addcallback(
								loop, 
								(struct GameLoopCallback){
									solarpanelincome,
									self 
								}, 
								1000
							);
						}
					}
					break;

				case PLAYERACTION_BUILDSOLDIER:
					if(self->power >= 25)
					{
						self->power -= 25;
						struct SoldierUnit soldier;
						soldierunit_ctor(
							&soldier, 
							rect_getpos(
								&self->player.unit.rect, 
								RECTREGPOINT_CENTER
							),
							game
						);
						soldier.isenemy = 0;
						vec_pushback(self->soldiers, soldier);
						vec_pushback(
							self->units, 
							&self->soldiers[vec_getsize(self->soldiers) - 1]
								.unit
						);
						vec_pushback(
							self->targetunits, 
							&self->soldiers[vec_getsize(self->soldiers) - 1]
								.unit
						);
					}
					break;
				}
				break;

			case CONTROLLERBUTTON_RIGHTSHOULDER: 
				self->units[self->selectedunit]->selected = 0;
				do 
				{
					self->selectedunit++;
					if(self->selectedunit >= vec_getsize(self->units))
					{
						self->selectedunit = 0;
					}
				}
				while(self->units[self->selectedunit]->isdead);

				self->units[self->selectedunit]->selected = 1;
				break;

			default:
				break;
			}
			break;

		case EVENTTYPE_KEYDOWN:
			switch(game->input.events[i].key)
			{
			case SDL_SCANCODE_TAB:
				self->units[self->selectedunit]->selected = 0;
				do 
				{
					self->selectedunit++;
					if(self->selectedunit >= vec_getsize(self->units))
					{
						self->selectedunit = 0;
					}
				}
				while(self->units[self->selectedunit]->isdead);

				self->units[self->selectedunit]->selected = 1;
				break;

			case SDL_SCANCODE_Q:
				if(self->player.unit.selected)
				{
					switch(self->action)
					{
					case PLAYERACTION_BUILDSOLAR:
						self->action = PLAYERACTION_BUILDSOLDIER;
						break;

					case PLAYERACTION_BUILDSOLDIER:
						self->action = PLAYERACTION_BUILDSOLAR;
						break;
					}
				}
				break;

			case SDL_SCANCODE_E:
				switch(self->action)
				{
				case PLAYERACTION_BUILDSOLAR:
					{
						struct SolarPanel panel = {0};
						rect_ctor(
							&panel.unit.rect, 
							rect_getpos(
								&self->player.unit.rect, 
								RECTREGPOINT_CENTER
							), 
							RECTREGPOINT_CENTER, 
							SOLARPANEL_WIDTH,
							SOLARPANEL_HEIGHT
						);

						panel.unit.texture = self->solarpanel;
						panel.unit.hp = SOLARPANEL_HP;
						panel.unit.maxhp = SOLARPANEL_HP;
						int hit = 0;
						for(size_t j = 0;
							j < vec_getsize(self->panels); 
							j++)
						{
							if(rect_intersects(
								&panel.unit.rect, 
								&self->panels[j].unit.rect))
							{
								hit = 1;
								break;
							}
						}

						if(!hit && self->power >= 15)
						{
							self->power -= 15;
							vec_pushback(self->panels, panel);
							gameloop_addcallback(
								loop, 
								(struct GameLoopCallback){
									solarpanelincome,
									self 
								}, 
								1000
							);
						}
					}
					break;

				case PLAYERACTION_BUILDSOLDIER:
					if(self->power >= 25)
					{
						self->power -= 25;
						struct SoldierUnit soldier;
						soldierunit_ctor(
							&soldier, 
							rect_getpos(
								&self->player.unit.rect, 
								RECTREGPOINT_CENTER
							),
							game
						);
						soldier.isenemy = 0;
						vec_pushback(self->soldiers, soldier);
						vec_pushback(
							self->units, 
							&self->soldiers[vec_getsize(self->soldiers) - 1]
								.unit
						);
					}
					break;
				}
				break;

			default:
				break;
			}
			break;

		default: 
			break;
		}
	}

	double angle;
	if(inputhandler_cjoystickangle(
		&game->input, 
		game->controller, 
		CONTROLLERJOYSTICK_LEFT, 
		&angle))
	{
		self->units[self->selectedunit]->vel.x = cos(angle);
		self->units[self->selectedunit]->vel.y = sin(angle);
	}
	
	if(self->player.unit.selected)
	{
		inputhandler_cjoystickangle(
			&game->input, 
			game->controller, 
			CONTROLLERJOYSTICK_RIGHT, 
			&self->player.shootangle
		);
	}

	for(size_t i = 0; i < vec_getsize(self->panels); i++)
	{
		unit_update(&self->panels[i].unit, loop->tickrate);
	}

	for(size_t i = 0; i < vec_getsize(self->soldiers); i++)
	{
		soldierunit_update(&self->soldiers[i], game, loop);
	}

	for(size_t i = 0; i < vec_getsize(game->projectiles); i++)
	{
		if(!game->projectiles[i].done && !game->projectiles[i].allied)
		{
			double x = game->projectiles[i].pos.x 
				+ cos(game->projectiles->rotation) * game->laser.width / 2.0;
			double y = game->projectiles[i].pos.y
				+ sin(game->projectiles->rotation) * game->laser.width / 2.0;

			if(rect_intersectspoint(&self->player.unit.rect, x, y))
			{
				inputhandler_cvibrate(&game->input, game->controller, 500);
				self->player.unit.hp -= 5;
				game->projectiles[i].done = 1;
			}
		}
	}

	for(size_t i = 0; i < vec_getsize(game->projectiles); i++)
	{
		if(!game->projectiles[i].done && !game->projectiles[i].allied)
		{
			double x = game->projectiles[i].pos.x 
				+ cos(game->projectiles->rotation) * game->laser.width / 2.0;
			double y = game->projectiles[i].pos.y
				+ sin(game->projectiles->rotation) * game->laser.width / 2.0;

			if(rect_intersectspoint(&self->player.unit.rect, x, y))
			{
				inputhandler_cvibrate(&game->input, game->controller, 500);
				self->player.unit.hp -= 5;
				game->projectiles[i].done = 1;
			}
			else
			{
				for(size_t j = 0; j < vec_getsize(self->panels); j++)
				{
					if(rect_intersectspoint(&self->panels[j].unit.rect, x, y))
					{
						self->panels[j].unit.hp -= 5;
						game->projectiles[i].done = 1;
					}
				}
			}
		}
	}

	unit_update(&self->player.unit, loop->tickrate);
	if(self->player.unit.isdead)
	{
		game->gameovermenu.win = 0;
		game->state = GAMESTATE_LOSE;
		window_grabmouse(&game->window, 0);
	}
}

void player_render(
	struct Player* self, 
	struct Game* game, 
	struct GameLoop* loop)
{
	log_assert(self, "is NULL");
	log_assert(game, "is NULL");
	log_assert(loop, "is NULL");

	for(size_t i = 0; i < vec_getsize(self->panels); i++)
	{
		window_setdrawcolor(&game->window, color(25, 25, 25, 255));
		unit_render(&self->panels[i].unit, game, loop);
	}

	for(size_t i = 0; i < vec_getsize(self->soldiers); i++)
	{
		soldierunit_render(&self->soldiers[i], game, loop);
	}

	window_setdrawcolor(&game->window, color(0, 0, 255, 255));
	window_setdrawcolor(&game->window, color(0, 0, 255, 255));
	unit_render(&self->player.unit, game, loop);

	struct Rect rect;
	rect_ctor(
		&rect, 
		(struct Vec2d){
			.x = self->units[self->selectedunit]->renderpos.x 
				+ self->units[self->selectedunit]->rect.width  / 2.0,
			.y = self->units[self->selectedunit]->renderpos.y 
				+ self->units[self->selectedunit]->rect.height / 2.0,
		},
		RECTREGPOINT_CENTER,
		self->units[self->selectedunit]->rect.width  * 1.5,
		self->units[self->selectedunit]->rect.height * 1.5
	);
	window_rendertexture(&game->window, &game->circle, NULL, &rect);
	window_setdrawcolor(&game->window, color(0, 0, 0, 170));
	window_renderfillrect(
		&game->window, 
		&(struct Rect){
			.pos = {.x = 0, .y = 0},
			.width = 415,
			.height = 60
		}
	);
	window_rendertexture(
		&game->window, 
		&self->powertext, 
		NULL, 
		&(struct Rect){
			.pos = {
				.x = 20,
				.y = 5,
			},
			.width = self->powertext.width,
			.height = self->powertext.height,
		}
	);

	if(self->player.unit.selected)
	{
		struct Vec2d pos1 = {
			.x = self->player.unit.renderpos.x 
				+ self->player.unit.rect.width  / 2.0,
			.y = self->player.unit.renderpos.y 
				+ self->player.unit.rect.height / 2.0,
		};
		struct Vec2d pos2 = pos1; 
		struct Vec2d temp = {
			.x = cos(self->player.shootangle),
			.y = sin(self->player.shootangle)
		};
		vec2d_normalize(&temp, &temp);
		vec2d_scale(&temp, 100, &temp);
		vec2d_add(&pos2, &temp, &pos2);
		window_setdrawcolor(&game->window, color(255, 0, 0, 255));
		window_renderline(&game->window, pos1, pos2);

		window_setdrawcolor(&game->window, color(0, 0, 0, 170));
		window_renderfillrect(
			&game->window, 
			&(struct Rect){
				.pos = {
					.x = 0,
					.y = game->window.height - 60
				},
				.width = 750,
				.height = 60
			}
		);

		button_render(&self->buildsolar, game);
		button_render(&self->buildsoldier, game);
	}
}

void player_dtor(struct Player* self)
{
	log_assert(self, "is NULL");

	vec_delete(self->soldiers); 
	vec_delete(self->panels);
}

