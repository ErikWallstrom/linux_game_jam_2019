#include "tilemap.h"
#include "../../log.h"
#include "game.h"

struct TileMap* tilemap_ctor(struct TileMap* self, struct Game* game)
{
	log_assert(self, "is NULL");

	self->map = vec_new(struct Tile, 0);
	texture_ctorimage(&self->tilestypes[0], "res/moon1.png", &game->window);
	texture_ctorimage(&self->tilestypes[1], "res/moon2.png", &game->window);
	texture_ctorimage(&self->tilestypes[2], "res/moon3.png", &game->window);
	texture_ctorimage(&self->tilestypes[3], "res/moon4.png", &game->window);

	for(size_t i = 0; i < TILES_X; i++)
	{
		for(size_t j = 0; j < TILES_Y; j++)
		{
			int type     = rand() % 4;
			int rotation = rand() % 4 * 90;

			vec_pushback(
				self->map, 
				(struct Tile){
					.type = type,
					.rotation = rotation				
				}
			);
		}
	}

	return self;
}

void tilemap_render(struct TileMap* self, struct Game* game)
{
	log_assert(self, "is NULL");
	
	for(size_t i = 0; i < TILES_X; i++)
	{
		for(size_t j = 0; j < TILES_X; j++)
		{
			window_renderrotatedtexture(
				&game->window,
				&self->tilestypes[self->map[i * j].type],
				NULL,
				&(struct Rect){
					.pos = {
						.x = j * TILE_WIDTH  * TILE_SCALE - game->camera
							.renderpos.x,
						.y = i * TILE_HEIGHT * TILE_SCALE - game->camera
							.renderpos.y,
					},
					TILE_WIDTH  * TILE_SCALE,
					TILE_HEIGHT * TILE_SCALE
				},
				self->map[i * j].rotation,
				RECTREGPOINT_CENTER
			);
		}
	}
}

void tilemap_dtor(struct TileMap* self)
{
	log_assert(self, "is NULL");
	
	vec_delete(self->map);
	texture_dtor(&self->tilestypes[0]);
	texture_dtor(&self->tilestypes[1]);
	texture_dtor(&self->tilestypes[2]);
	texture_dtor(&self->tilestypes[3]);
}

