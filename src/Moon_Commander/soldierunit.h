#ifndef SOLDIERUNIT_H
#define SOLDIERUNIT_H

#include "button.h"
#include "unit.h"

enum SoldierCommand
{
	SOLDIERCOMMAND_OFFENSIVE,
	SOLDIERCOMMAND_FOLLOW,
	SOLDIERCOMMAND_SHOOT,
};

struct SoldierUnit
{
	struct Button shootbutton;
	struct Button followbutton;
	struct Button offensivebutton;
	struct Unit unit;
	struct Unit* movetarget;
	double shootangle;
	enum SoldierCommand command;
	int isenemy;
};

struct SoldierUnit* soldierunit_ctor(
	struct SoldierUnit* self, 
	struct Vec2d pos,
	struct Game* game
);
void soldierunit_update(
	struct SoldierUnit* self, 
	struct Game* game,
	struct GameLoop* loop
);
void soldierunit_shoot(struct SoldierUnit* self, struct Game* game);
void soldierunit_render(
	struct SoldierUnit* self, 
	struct Game* game, 
	struct GameLoop* loop
);

#endif
