#ifndef CAMERA_H
#define CAMERA_H

#include "../../vec2d.h"

struct Game;
struct GameLoop;
struct Camera
{
	struct Vec2d renderpos;
	struct Vec2d pos;
	struct Vec2d oldpos;
	struct Vec2d vel;
	struct Vec2d start;
	struct Vec2d goal;
	double speed;
	int movetowardsgoal;
};

struct Camera* camera_ctor(struct Camera* self, struct Vec2d pos);
void camera_update(
	struct Camera* self, 
	struct Game* game, 
	struct GameLoop* loop
);
void camera_render(
	struct Camera* self, 
	struct Game* game, 
	struct GameLoop* loop
);

#endif
