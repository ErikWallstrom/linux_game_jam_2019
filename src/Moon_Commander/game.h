#ifndef GAME_H
#define GAME_H

#include "../../inputhandler.h"
#include "../../texture.h"
#include "../../window.h"
#include "../../vec2d.h"
#include "../../font.h"

#include "gameovermenu.h"
#include "projectile.h"
#include "pausemenu.h"
#include "mainmenu.h"
#include "tilemap.h"
#include "player.h"
#include "camera.h"

enum GameState
{
	GAMESTATE_MENU,
	GAMESTATE_NORMAL,
	GAMESTATE_WIN,
	GAMESTATE_LOSE,
};

struct Game
{
	struct Window window;
	struct InputHandler input;
	struct Player player;
	struct Camera camera;
	struct Font font;
	struct TileMap tilemap;
	Vec(struct Projectile) projectiles;
	Vec(struct SoldierUnit) enemies;

	//Textures
	struct Texture circle;
	struct Texture laser;
	struct Texture gameovertext;
	struct Texture offensive;
	struct Texture follow;
	struct Texture shoot;
	struct Texture wave;

	//Menus
	struct MainMenu mainmenu;
	struct PauseMenu pausemenu;
	struct GameOverMenu gameovermenu;

	int wavenum;
	int controller;
	int paused;
	enum GameState state;
};

void game_reset(struct Game* game, struct GameLoop* loop);

#endif
