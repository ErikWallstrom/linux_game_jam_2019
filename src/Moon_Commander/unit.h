#ifndef UNIT_H
#define UNIT_H

#include "../../texture.h"
#include "../../rect.h"
#include "../../vec.h"

struct Game;
struct GameLoop;

struct Unit
{
	struct Texture texture;
	struct Rect rect;
	struct Vec2d renderpos;
	struct Vec2d oldpos;
	struct Vec2d vel;
	struct Vec2d goal;
	double speed;
	int selected;
	int maxhp;
	int hp;
	int isdead;
};

void unit_update(struct Unit* self, double tickrate);
void unit_render(struct Unit* self, struct Game* game, struct GameLoop* loop);

#endif
