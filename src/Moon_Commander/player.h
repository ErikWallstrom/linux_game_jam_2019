#ifndef PLAYER_H
#define PLAYER_H

#include "soldierunit.h"

struct SolarPanel
{
	struct Unit unit;
};

struct PlayerUnit
{
	struct Unit unit;
	double shootangle;
};

enum PlayerAction
{
	PLAYERACTION_BUILDSOLAR,
	PLAYERACTION_BUILDSOLDIER,
};

struct Player
{
	struct PlayerUnit player;
	struct Texture solarpanel;
	struct Texture powertext;
	struct Texture solartext;
	struct Texture soldiertext;
	struct Button buildsolar;
	struct Button buildsoldier;
	Vec(struct SoldierUnit) soldiers;
	Vec(struct SolarPanel) panels;
	Vec(struct Unit*) targetunits;
	Vec(struct Unit*) units;
	size_t selectedunit;
	int power;
	int powerdisplayed;
	enum PlayerAction action;
};

struct Player* player_ctor(
	struct Player* self, 
	struct Vec2d pos,
	struct Game* game,
	struct GameLoop* loop
);
void player_update(
	struct Player* self, 
	struct Game* game, 
	struct GameLoop* loop
);
void player_render(
	struct Player* self, 
	struct Game* game, 
	struct GameLoop* loop
);
void player_dtor(struct Player* self);

#endif
