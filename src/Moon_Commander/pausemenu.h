#ifndef PAUSEMENU_H
#define PAUSEMENU_H

#include "button.h"

struct Game;
struct GameLoop;

struct PauseMenu
{
	struct Button resume;
	struct Button mainmenu;
	struct Rect rect;
	int selectedbutton;
};

struct PauseMenu* pausemenu_ctor(
	struct PauseMenu* self, 
	struct Game* game
);
void pausemenu_update(
	struct PauseMenu* self, 
	struct Game* game, 
	struct GameLoop* loop
);
void pausemenu_render(
	struct PauseMenu* self, 
	struct Game* game, 
	struct GameLoop* loop
);
void pausemenu_dtor(struct PauseMenu* self);

#endif
