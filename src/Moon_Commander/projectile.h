#ifndef PROJECTILE_H
#define PROJECTILE_H

#include "../../texture.h"
#include "../../rect.h"

enum ProjectileType
{
	PROJECTILETYPE_LASER,
	PROJECTILETYPE_EXPLOSIVE,
};

struct Game;
struct GameLoop;

struct Projectile
{
	struct Vec2d pos;
	struct Vec2d oldpos;
	struct Vec2d start;
	double rotation;
	int done;
	int allied;
	enum ProjectileType type;
};

void projectile_update(
	struct Projectile* self, 
	struct Game* game,
	struct GameLoop* loop
);
void projectile_render(
	struct Projectile* self, 
	struct Game* game, 
	struct GameLoop* loop
);

#endif
