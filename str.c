#include "str.h"
#include "log.h"
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <ctype.h>

int strslice_equal(struct StrSlice s1, struct StrSlice s2)
{
	if(s1.len != s2.len)
	{
		return 0;
	}

	return !memcmp(s1.data, s2.data, s1.len);
}

struct StrSlice strslice_empty(void)
{
	return (struct StrSlice){.len = 0, .data = NULL};
}

int strslice_isempty(struct StrSlice s)
{
	return s.data == NULL && s.len == 0;
}

struct Str* str_ctor(struct Str* self, const char* str)
{ 
	log_assert(self, "is NULL");
	log_assert(str, "is NULL");

	size_t len = strlen(str);
	str_ctorbuffer(self, len);
	memcpy(self->data, str, len + 1);
	return self;
}

struct Str* str_ctorbuffer(struct Str* self, size_t len)
{
	log_assert(self, "is NULL");

	self->data = malloc(len + 1);
	if(!self->data)
	{ 
		log_error("malloc failed in <%s>", __func__);
	}

	self->len = len;
	return self;
}

struct Str* str_fromslice(struct Str* self, struct StrSlice s)
{
	log_assert(self, "is NULL");
	log_assert(!strslice_isempty(s), "empty slice");

	str_ctorbuffer(self, s.len);
	memcpy(self->data, s.data, s.len);
	self->data[s.len] = '\0';
	return self;
}

struct StrSlice str_toslice(struct Str* self)
{
	log_assert(self, "is NULL");
	return (struct StrSlice){.len = self->len, .data = self->data};
}

__attribute__((format (printf, 2, 3)))
struct Str* str_ctorfmt(struct Str* self, const char* fmt, ...) 
{ 
	log_assert(fmt, "is NULL");

	va_list vlist1, vlist2;
	va_start(vlist1, fmt);
	va_copy(vlist2, vlist1);

	size_t len = vsnprintf(NULL, 0, fmt, vlist1);
	self->data = malloc(len + 1);
	if(!self->data)
	{ 
		log_error("malloc failed in <%s>", __func__);
	}

	vsprintf(self->data, fmt, vlist2);
	self->len = len;

	va_end(vlist2);
	va_end(vlist1);
	return self;
}

size_t str_insert(struct Str* self, size_t index, const char* str)
{ 
	log_assert(self, "is NULL");
	log_assert(index <= self->len, "is NULL");
	log_assert(str, "is NULL");

	size_t len = strlen(str);
	self->data = realloc(self->data, self->len + len + 1);
	if(!self->data)
	{ 
		log_error("realloc failed in <%s>", __func__);
	}

	memmove(
		self->data + index + len, 
		self->data + index, 
		self->len - index + 1
	);

	memcpy(self->data + index, str, len);
	self->len += len;

	return index + len;
}

static size_t str_insertfmtva(
	struct Str* self, 
	size_t index, 
	const char* fmt, 
	va_list vlist)
{ 
	va_list vlist2;
	va_copy(vlist2, vlist);

	size_t len = vsnprintf(NULL, 0, fmt, vlist);
	self->data = realloc(self->data, self->len + len + 1);
	if(!self->data)
	{ 
		log_error("realloc failed in <%s>", __func__);
	}

	memmove(
		self->data + index + len, 
		self->data + index, 
		self->len - index + 1
	);

	char* buffer = malloc(len + 1);
	if(!buffer)
	{ 
		log_error("malloc failed in <%s>", __func__);
	}

	vsprintf(buffer, fmt, vlist2);
	memcpy(self->data + index, buffer, len);
	free(buffer);
	self->len += len;

	va_end(vlist2);
	return index + len;
}

__attribute__((format (printf, 3, 4)))
size_t str_insertfmt(struct Str* self, size_t index, const char* fmt, ...)
{ 
	log_assert(self, "is NULL");
	log_assert(index <= self->len, "is NULL");
	log_assert(fmt, "is NULL");

	va_list vlist;
	va_start(vlist, fmt);
	size_t ret = str_insertfmtva(self, index, fmt, vlist);
	va_end(vlist);

	return ret;
}

void str_append(struct Str* self, const char* str)
{ 
	log_assert(self, "is NULL");
	log_assert(str, "is NULL");

	str_insert(self, self->len, str);
}

__attribute__((format (printf, 2, 3)))
void str_appendfmt(struct Str* self, const char* fmt, ...)
{ 
	log_assert(self, "is NULL");
	log_assert(fmt, "is NULL");

	va_list vlist;
	va_start(vlist, fmt);
	str_insertfmtva(self, self->len, fmt, vlist);
	va_end(vlist);
}

size_t str_prepend(struct Str* self, const char* str)
{ 
	log_assert(self, "is NULL");
	log_assert(str, "is NULL");

	return str_insert(self, 0, str);
}

__attribute__((format (printf, 2, 3)))
size_t str_prependfmt(struct Str* self, const char* fmt, ...)
{ 
	log_assert(self, "is NULL");
	log_assert(fmt, "is NULL");

	va_list vlist;
	va_start(vlist, fmt);
	size_t ret = str_insertfmtva(self, 0, fmt, vlist);
	va_end(vlist);

	return ret;
}

int str_equal(struct Str* s1, struct Str* s2)
{
	if(s1->len != s2->len)
	{
		return 0;
	}

	return !memcmp(s1->data, s2->data, s1->len);
}

void str_lower(struct Str* self)
{
	log_assert(self, "is NULL");
	for(size_t i = 0; i < self->len; i++)
	{
		self->data[i] = tolower(self->data[i]);
	}
}

void str_upper(struct Str* self)
{
	log_assert(self, "is NULL");
	for(size_t i = 0; i < self->len; i++)
	{
		self->data[i] = toupper(self->data[i]);
	}
}

void str_dtor(struct Str* self)
{ 
	log_assert(self, "is NULL");
	free(self->data);
}

