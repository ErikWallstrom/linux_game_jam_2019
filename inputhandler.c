#include "inputhandler.h"
#include "log.h"
#include <SDL2/SDL.h>

struct InputHandler* inputhandler_ctor(struct InputHandler* self)
{
	log_assert(self, "is NULL");

	//TODO: Change 0 to a more appropriate value
	self->events = vec_new(struct Event, 0); 
	self->keystate = SDL_GetKeyboardState(NULL);
	self->mousestate = SDL_GetMouseState(&self->mousex, &self->mousey);
	self->controllers = vec_new(void*, 0);

	self->mousex = 1; //Make it not think mouse is at (0,0) at launch
	self->mousey = 1;

	if(!SDL_NumJoysticks())
	{
		log_warning("No compatible game controllers available");
	}

	return self;
}

void inputhandler_update(struct InputHandler* self)
{
	log_assert(self, "is NULL");
	vec_clear(self->events);

	struct Event temp;
	SDL_Event event;
	while(SDL_PollEvent(&event))
	{
		switch(event.type)
		{
		case SDL_CONTROLLERAXISMOTION:
			temp.type = EVENTTYPE_AXISMOTION;
			temp.axismotion.value = event.caxis.value;
			switch(event.caxis.axis)
			{
			case SDL_CONTROLLER_AXIS_LEFTX:
				temp.axismotion.axis = CONTROLLERAXIS_LEFTX;
				break;

			case SDL_CONTROLLER_AXIS_RIGHTX:
				temp.axismotion.axis = CONTROLLERAXIS_RIGHTX;
				break;

			case SDL_CONTROLLER_AXIS_LEFTY:
				temp.axismotion.axis = CONTROLLERAXIS_LEFTY;
				break;

			case SDL_CONTROLLER_AXIS_RIGHTY:
				temp.axismotion.axis = CONTROLLERAXIS_RIGHTY;
				break;

			case SDL_CONTROLLER_AXIS_TRIGGERLEFT:
				temp.axismotion.axis = CONTROLLERAXIS_TRIGGERLEFT;
				break;

			case SDL_CONTROLLER_AXIS_TRIGGERRIGHT:
				temp.axismotion.axis = CONTROLLERAXIS_TRIGGERRIGHT;
				break;

				//Handle unknown?
			}

			vec_pushback(self->events, temp);
			break;

		case SDL_MOUSEMOTION:
			temp.type = EVENTTYPE_MOUSEMOTION;
			temp.mousemotion.x = event.motion.xrel;
			temp.mousemotion.y = event.motion.yrel;
			self->mousex = event.motion.x;
			self->mousey = event.motion.y;
			vec_pushback(self->events, temp);
			break;

		case SDL_MOUSEWHEEL:
			temp.type = EVENTTYPE_MOUSEWHEEL;
			if(event.wheel.y)
			{
				temp.wheelmotion.value = event.wheel.y;
				vec_pushback(self->events, temp);
			}
			break;

		case SDL_KEYDOWN:
			if(!event.key.repeat)
			{
				temp.type = EVENTTYPE_KEYDOWN;
				temp.key = event.key.keysym.scancode;
				vec_pushback(self->events, temp);
			}
			break;

		case SDL_KEYUP:
			if(!event.key.repeat)
			{
				temp.type = EVENTTYPE_KEYUP;
				temp.key = event.key.keysym.scancode;
				vec_pushback(self->events, temp);
			}
			break;

		case SDL_CONTROLLERBUTTONDOWN:
			temp.type = EVENTTYPE_BUTTONDOWN;
			temp.cbutton = event.cbutton.button;
			vec_pushback(self->events, temp);
			break;

		case SDL_CONTROLLERBUTTONUP:
			temp.type = EVENTTYPE_BUTTONUP;
			temp.cbutton = event.cbutton.button;
			vec_pushback(self->events, temp);
			break;

		case SDL_QUIT:
			temp.type = EVENTTYPE_QUIT;
			vec_pushback(self->events, temp);
			break;

		case SDL_CONTROLLERDEVICEADDED:
			{
				SDL_GameController* controller = SDL_GameControllerOpen(
					event.cdevice.which
				);

				if(controller && SDL_GameControllerMapping(controller))
				{
					vec_pushback(self->controllers, controller);
				}
				else
				{
					log_warning(
						"Unable to open controller #%i, '%s' (%s)", 
						event.cdevice.which,
						SDL_GameControllerNameForIndex(event.cdevice.which),
						SDL_GetError()
					);
				}
			}
			break;

		case SDL_CONTROLLERDEVICEREMOVED:;
			//TODO
			log_warning(
				"Controller #%i, '%s' removed", 
				event.cdevice.which,
				SDL_GameControllerNameForIndex(event.cdevice.which)
			);
			break;

		case SDL_MOUSEBUTTONDOWN:
			temp.type = EVENTTYPE_MOUSEDOWN;
			switch(event.button.button)
			{
			case SDL_BUTTON_LEFT:
				temp.mbutton = MOUSEBUTTON_LEFT;
				break;

			case SDL_BUTTON_MIDDLE:
				temp.mbutton = MOUSEBUTTON_MIDDLE;
				break;

			case SDL_BUTTON_RIGHT:
				temp.mbutton = MOUSEBUTTON_RIGHT;
				break;
			}

			vec_pushback(self->events, temp);
			break;

		case SDL_MOUSEBUTTONUP:
			temp.type = EVENTTYPE_MOUSEUP;
			temp.mbutton = event.cbutton.button;
			vec_pushback(self->events, temp);
			break;

		default:
			break;
		}
	}

	self->mousestate = SDL_GetMouseState(NULL, NULL);
}

int inputhandler_cjoystickangle(
	struct InputHandler* self, 
	int controller,
	enum ControllerJoystick joystick,
	double* angle)
{
	log_assert(self, "is NULL");
	log_assert(angle, "is NULL");

	if(controller == -1)
	{
		return 0;
	}

	SDL_GameControllerAxis axisx = joystick == CONTROLLERJOYSTICK_LEFT 
		? SDL_CONTROLLER_AXIS_LEFTX 
		: SDL_CONTROLLER_AXIS_RIGHTX;
	SDL_GameControllerAxis axisy = joystick == CONTROLLERJOYSTICK_LEFT 
		? SDL_CONTROLLER_AXIS_LEFTY 
		: SDL_CONTROLLER_AXIS_RIGHTY;

	int16_t x = SDL_GameControllerGetAxis(self->controllers[controller], axisx);
	int16_t y = SDL_GameControllerGetAxis(self->controllers[controller], axisy);
	if(y || x)
	{
		*angle = atan2(y, x);
		return 1;
	}

	return 0;
}

int inputhandler_cgetaxis(
	struct InputHandler* self, 
	int controller, 
	enum ControllerAxis axis)
{
	log_assert(self, "is NULL");
	
	if(controller == -1)
	{
		return 0;
	}

	return SDL_GameControllerGetAxis(self->controllers[controller], axis);
}

int inputhandler_cbuttondown(
	struct InputHandler* self, 
	int controller, 
	enum ControllerButton button)
{
	log_assert(self, "is NULL");

	if(controller == -1)
	{
		return 0;
	}
	
	return SDL_GameControllerGetButton(self->controllers[controller], button);
}

void inputhandler_cvibrate(
	struct InputHandler* self, 
	int controller, 
	size_t duration)
{
	log_assert(self, "is NULL");
	log_assert(duration, "is 0");

	if(controller == -1)
	{
		return;
	}

	if(SDL_GameControllerRumble(
		self->controllers[controller], 
		0xFFFF, 
		0xFFFF, 
		duration))
	{
		log_warning("%s", SDL_GetError());
	}
}

void inputhandler_dtor(struct InputHandler* self)
{
	log_assert(self, "is NULL");

	for(size_t i = 0; i < vec_getsize(self->controllers); i++)
	{
		SDL_GameControllerClose(self->controllers[i]);
	}

	vec_delete(self->events);
}

