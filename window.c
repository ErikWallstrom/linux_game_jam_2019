#include "window.h"
#include "rect.h"
#include "texture.h"
#include <SDL2/SDL.h>
#include "log.h"

struct Window* window_ctor(
	struct Window* self, 
	const char* title, 
	int display,
	int driver,
	int width, 
	int height,
	enum WindowFlags flags)
{
	log_assert(self, "is NULL");
	log_assert(title, "is NULL");
	log_assert(display >= 0, "invalid display");
	log_assert(width   >  0, "invalid width");
	log_assert(height  >  0, "invalid height");

	int windowflags = SDL_WINDOW_SHOWN;
	int renderflags = SDL_RENDERER_ACCELERATED | SDL_RENDERER_TARGETTEXTURE;
	if(flags & WINDOW_RESIZABLE)
	{
		windowflags |= SDL_WINDOW_RESIZABLE;
	}

	if(flags & WINDOW_FULLSCREEN)
	{
		windowflags |= SDL_WINDOW_FULLSCREEN;
		SDL_SetHint(SDL_HINT_VIDEO_MINIMIZE_ON_FOCUS_LOSS, "0");
	}

	if(flags & WINDOW_FULLSCREENDESKTOP)
	{
		windowflags |= SDL_WINDOW_FULLSCREEN_DESKTOP;
		SDL_SetHint(SDL_HINT_VIDEO_MINIMIZE_ON_FOCUS_LOSS, "0");
	}

	if(flags & WINDOW_VSYNC)
	{
		renderflags |= SDL_RENDERER_PRESENTVSYNC;
	}

	self->raw = SDL_CreateWindow(
		title, 
		SDL_WINDOWPOS_CENTERED_DISPLAY(display),
		SDL_WINDOWPOS_CENTERED_DISPLAY(display),
		width, 
		height, 
		windowflags
	);
	if(!self->raw)
	{
		log_error("%s", SDL_GetError());
	}

	self->renderer = SDL_CreateRenderer(self->raw, driver, renderflags);
	if(!self->renderer)
	{
		log_error("%s", SDL_GetError());
	}

	if(SDL_SetRenderDrawBlendMode(self->renderer, SDL_BLENDMODE_BLEND))
	{
		log_error("%s", SDL_GetError());
	}

	if(flags & WINDOW_FULLSCREEN || flags & WINDOW_FULLSCREENDESKTOP)
	{
		SDL_RenderSetLogicalSize(self->renderer, width, height);
	}

	self->title = SDL_GetWindowTitle(self->raw);
	self->flags = flags;
	self->width = width;
	self->height = height;
	self->color = color(0, 0, 0, 0);

	return self;
}

void window_setwidth(struct Window* self, int width)
{
	log_assert(self, "is NULL");
	log_assert(width > 0, "invalid size %i", width);

	self->width = width;
	SDL_SetWindowSize(self->raw, width, self->height);
}

void window_setheight(struct Window* self, int height)
{
	log_assert(self, "is NULL");
	log_assert(height > 0, "invalid size %i", height);

	self->height = height;
	SDL_SetWindowSize(self->raw, self->width, height);
}

void window_settitle(struct Window* self, const char* title)
{
	log_assert(self, "is NULL");
	log_assert(title, "is NULL");
	
	self->title = title;
	SDL_SetWindowTitle(self->raw, title);
}

void window_rendertexture(
	struct Window* self, 
	struct Texture* texture, 
	struct Rect* srect, 
	struct Rect* drect)
{
	log_assert(self, "is NULL");
	log_assert(texture, "is NULL");
	log_assert(drect, "is NULL");

	SDL_Rect srect_;
	if(srect)
	{
		srect_ = (SDL_Rect){
			.x = srect->pos.x,
			.y = srect->pos.y,
			.w = srect->width,
			.h = srect->height
		};
	}

	SDL_Rect drect_ = {
		.x = drect->pos.x,
		.y = drect->pos.y,
		.w = drect->width,
		.h = drect->height
	};

	SDL_RenderCopy(
		self->renderer,
		texture->raw,
		srect ? &srect_ : NULL,
		&drect_
	);
}

void window_renderrotatedtexture(
	struct Window* self, 
	struct Texture* texture, 
	struct Rect* srect, 
	struct Rect* drect,
	double angle,
	enum RectRegPoint center)
{
	log_assert(self, "is NULL");
	log_assert(texture, "is NULL");
	log_assert(drect, "is NULL");

	SDL_Rect srect_;
	if(srect)
	{
		srect_ = (SDL_Rect){
			.x = srect->pos.x,
			.y = srect->pos.y,
			.w = srect->width,
			.h = srect->height
		};
	}

	SDL_Rect drect_ = {
		.x = drect->pos.x,
		.y = drect->pos.y,
		.w = drect->width,
		.h = drect->height
	};

	SDL_Point point;
	switch(center)
	{
	case RECTREGPOINT_CENTER:
		point.x = drect->width / 2;
		point.y = drect->height / 2;
		break;
		
	default:
		log_assert(0, "Unhandled");
		break;
	}

	SDL_RenderCopyEx(
		self->renderer,
		texture->raw,
		srect ? &srect_ : NULL,
		&drect_,
		angle,
		&point,
		SDL_FLIP_NONE
	);
}

void window_renderrect(struct Window* self, struct Rect* rect)
{
	log_assert(self, "is NULL");
	log_assert(rect, "is NULL");

	SDL_Rect r = {
		.x = rect->pos.x,
		.y = rect->pos.y,
		.w = rect->width,
		.h = rect->height
	};
	
	SDL_RenderDrawRect(self->renderer, &r);
}

void window_renderline(struct Window* self, struct Vec2d v1, struct Vec2d v2)
{
	log_assert(self, "is NULL");
	SDL_RenderDrawLine(self->renderer, v1.x, v1.y, v2.x, v2.y);
}

void window_renderfillrect(struct Window* self, struct Rect* rect)
{
	log_assert(self, "is NULL");
	log_assert(rect, "is NULL");

	SDL_Rect r = {
		.x = rect->pos.x,
		.y = rect->pos.y,
		.w = rect->width,
		.h = rect->height
	};
	
	SDL_RenderFillRect(self->renderer, &r);
}

void window_clear(struct Window* self)
{
	log_assert(self, "is NULL");
	SDL_RenderClear(self->renderer);
}

void window_present(struct Window* self)
{
	log_assert(self, "is NULL");

	SDL_RenderPresent(self->renderer);
	if(SDL_GetWindowFlags(self->raw) & SDL_WINDOW_MINIMIZED)
	{
		SDL_Delay(1000 / 60); //Limit framerate while minimized (fix SDL bug)
	}
}

void window_setdrawcolor(struct Window* self, struct Color color)
{
	log_assert(self, "is NULL");
	
	self->color = color;
	SDL_SetRenderDrawColor(self->renderer, color.r, color.g, color.b, color.a);
}

void window_grabmouse(struct Window* self, int enabled)
{
	log_assert(self, "is NULL");
	SDL_SetWindowGrab(self->raw, enabled);
}

void window_dtor(struct Window* self)
{
	log_assert(self, "is NULL");

	SDL_DestroyRenderer(self->renderer);
	SDL_DestroyWindow(self->raw);
}

