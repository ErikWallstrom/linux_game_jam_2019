#ifndef WINDOW_H
#define WINDOW_H

#include "rect.h"
#include "color.h"

enum WindowFlags
{
	WINDOW_DEFAULT	         = 0,
	WINDOW_VSYNC 	         = 1 << 0,
	WINDOW_RESIZABLE         = 1 << 1,
	WINDOW_FULLSCREEN        = 1 << 2,
	WINDOW_FULLSCREENDESKTOP = 1 << 3,
};

struct Window
{
	struct Color color;
	void* raw;
	void* renderer;
	const char* title;
	int width, height;
	int flags;
};

struct Rect;
struct Texture;
struct Window* window_ctor(
	struct Window* self, 
	const char* title, 
	int display, 
	int driver, 
	int width, 
	int height,
	enum WindowFlags flags
);
void window_setwidth(struct Window* self, int width);
void window_setheight(struct Window* self, int height);
void window_settitle(struct Window* self, const char* title);
void window_setdrawcolor(struct Window* self, struct Color color);
void window_clear(struct Window* self);
void window_rendertexture(
	struct Window* self, 
	struct Texture* texture, 
	struct Rect* srect, 
	struct Rect* drect
);
void window_renderrotatedtexture(
	struct Window* self, 
	struct Texture* texture, 
	struct Rect* srect, 
	struct Rect* drect,
	double angle,
	enum RectRegPoint center
);
void window_renderline(struct Window* self, struct Vec2d v1, struct Vec2d v2);
void window_renderrect(struct Window* self, struct Rect* rect);
void window_renderfillrect(struct Window* self, struct Rect* rect);
void window_grabmouse(struct Window* self, int enabled);
void window_present(struct Window* self);
void window_dtor(struct Window* self);

#endif
