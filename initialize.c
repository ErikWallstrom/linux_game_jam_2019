#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_mixer.h>
#include <time.h>
#include "window.h"
#include "log.h"

void initialize(void)
{
	srand(time(NULL));
	SDL_version compileversion;
	SDL_version linkversion;

	SDL_VERSION(&compileversion);
	SDL_GetVersion(&linkversion);
	if(compileversion.major != linkversion.major 
		|| compileversion.minor != linkversion.minor 
		|| compileversion.patch != linkversion.patch)
	{
		log_warning(
			"Program was compiled with SDL version %i.%i.%i, but was linked"
				" with version %i.%i.%i\n",
			compileversion.major,
			compileversion.minor,
			compileversion.patch,
			linkversion.major,
			linkversion.minor,
			linkversion.patch
		);
	}

	if(SDL_Init(SDL_INIT_EVERYTHING)) //Is this enough?
	{
		log_error("%s", SDL_GetError());
	}

	const SDL_version* ttflinkversion = TTF_Linked_Version();
	SDL_TTF_VERSION(&compileversion);
	if(compileversion.major != ttflinkversion->major 
		|| compileversion.minor != ttflinkversion->minor 
		|| compileversion.patch != ttflinkversion->patch)
	{
		log_warning(
			"Program was compiled with SDL_ttf version %i.%i.%i, but was linked"
				" with version %i.%i.%i\n",
			compileversion.major,
			compileversion.minor,
			compileversion.patch,
			ttflinkversion->major,
			ttflinkversion->minor,
			ttflinkversion->patch
		);
	}

	if(TTF_Init())
	{
		log_error("%s", TTF_GetError());
	}

	const SDL_version* mixlinkversion = Mix_Linked_Version();
	MIX_VERSION(&compileversion);
	if(compileversion.major != mixlinkversion->major 
		|| compileversion.minor != mixlinkversion->minor 
		|| compileversion.patch != mixlinkversion->patch)
	{
		log_warning(
			"Program was compiled with SDL_mixer version %i.%i.%i, but was"
				" linked with version %i.%i.%i\n",
			compileversion.major,
			compileversion.minor,
			compileversion.patch,
			ttflinkversion->major,
			ttflinkversion->minor,
			ttflinkversion->patch
		);
	}

	if(!Mix_Init(MIX_INIT_MP3))
	{
		log_error("%s", Mix_GetError());
	}

	if(Mix_OpenAudio(MIX_DEFAULT_FREQUENCY, MIX_DEFAULT_FORMAT, 2, 4096))
	{
		log_error("%s", Mix_GetError());
	}
}

void cleanup(void)
{
	Mix_CloseAudio();
	Mix_Quit();
	TTF_Quit();
	SDL_Quit();
}

