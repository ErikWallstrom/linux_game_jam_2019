#include "file.h"
#include "log.h"
#include <string.h>
#include <errno.h>

struct File* file_ctor(
	struct File* self, 
	struct StrSlice path, 
	enum FileMode mode)
{
	log_assert(self, "is NULL");
	log_assert(
		mode & FILEMODE_READ || mode & FILEMODE_WRITE, 
		"invalid mode (%i)",
		mode
	);

	char* m;
	if(mode & FILEMODE_READ && mode & FILEMODE_WRITE)
	{
		m = "r+";
	}
	else if(mode & FILEMODE_READ)
	{
		m = "r";
	}
	else if(mode & FILEMODE_WRITE)
	{
		m = "wx"; //NOTE: 'x' (C11) makes fopen fail if file already exists
	}
	else //mode & FILEMODE_OVERWRITE
	{
		m = "w";
	}

	str_fromslice(&self->path, path);
	self->mode = mode;
	self->raw = fopen(self->path.data, m);
	if(!self->raw)
	{
		log_error("%s: '%.*s'", strerror(errno), (int)path.len, path.data);
	}

	if(mode & FILEMODE_READ)
	{
		if(fseek(self->raw, 0, SEEK_END))
		{
			log_error("%s", strerror(errno));
		}

		long filesize = ftell(self->raw);
		if(filesize == EOF)
		{
			log_error("%s", strerror(errno));
		}

		if(fseek(self->raw, 0, SEEK_SET))
		{
			log_error("%s", strerror(errno));
		}

		str_ctorbuffer(&self->content, filesize + 1);
		self->content.data[filesize] = '\0';
		if(filesize)
		{
			fread(self->content.data, 1, filesize, self->raw);
		}
	}
	else
	{
		str_ctor(&self->content, "");
	}

	for(size_t i = 0; i <= path.len; i++)
	{
		self->extension = path.data + i;
		if(path.data[i] == '.')
		{
			break;
		}
	}

	return self;
}

int file_exists(const char* path)
{
	FILE* file = fopen(path, "r");
	if(file)
	{
		fclose(file);
		return 1;
	}

	return 0;
}

void file_flush(struct File* self)
{
	log_assert(self, "is NULL");

	if(fseek(self->raw, 0, SEEK_SET))
	{
		log_error("%s", strerror(errno));
	}

	if(fputs(self->content.data, self->raw) == EOF)
	{
		log_error("%s", strerror(errno));
	}

	if(fflush(self->raw) == EOF)
	{
		log_error("%s", strerror(errno));
	}
}

void file_dtor(struct File* self)
{
	log_assert(self, "is NULL");
	if(self->mode & FILEMODE_WRITE)
	{
		file_flush(self);
	}

	str_dtor(&self->content);
	str_dtor(&self->path);
	fclose(self->raw);
}

