#ifndef INPUTHANDLER_H
#define INPUTHANDLER_H

#include "inttypes.h"
#include "vec.h"
#include <SDL2/SDL_scancode.h>
#include <SDL2/SDL_keycode.h>

enum EventType
{
	EVENTTYPE_MOUSEMOTION,
	EVENTTYPE_MOUSEWHEEL,
	EVENTTYPE_MOUSEDOWN,
	EVENTTYPE_MOUSEUP,
	EVENTTYPE_KEYDOWN,
	EVENTTYPE_KEYUP,
	EVENTTYPE_AXISMOTION,
	EVENTTYPE_BUTTONDOWN,
	EVENTTYPE_BUTTONUP,
	EVENTTYPE_QUIT,
};

enum ControllerJoystick
{
	CONTROLLERJOYSTICK_LEFT,
	CONTROLLERJOYSTICK_RIGHT
};

enum ControllerAxisValue
{
	CONTROLLERAXIS_MAX = -32768,
	CONTROLLERAXIS_MIN =  32767
};

enum ControllerAxis
{
	CONTROLLERAXIS_TRIGGERLEFT = 0,
	CONTROLLERAXIS_TRIGGERRIGHT,
	CONTROLLERAXIS_LEFTX,
	CONTROLLERAXIS_LEFTY,
	CONTROLLERAXIS_RIGHTX,
	CONTROLLERAXIS_RIGHTY,
};

enum ControllerButton
{
	CONTROLLERBUTTON_A = 0,
	CONTROLLERBUTTON_B,
	CONTROLLERBUTTON_X,
	CONTROLLERBUTTON_Y,
	CONTROLLERBUTTON_BACK,
	CONTROLLERBUTTON_GUIDE,
	CONTROLLERBUTTON_START,
	CONTROLLERBUTTON_LEFTSTICK,
	CONTROLLERBUTTON_RIHTSTICK,
	CONTROLLERBUTTON_LEFTSHOULDER,
	CONTROLLERBUTTON_RIGHTSHOULDER,
	CONTROLLERBUTTON_UP,
	CONTROLLERBUTTON_DOWN,
	CONTROLLERBUTTON_LEFT,
	CONTROLLERBUTTON_RIGHT
};

enum MouseButton
{
	MOUSEBUTTON_LEFT,
	MOUSEBUTTON_MIDDLE,
	MOUSEBUTTON_RIGHT,
};

struct Event
{
	union
	{
		struct 
		{
			enum ControllerAxis axis;
			int16_t value;
		} axismotion;

		struct
		{
			int32_t value;
		} wheelmotion;

		struct
		{
			int32_t x;
			int32_t y;
		} mousemotion;

		SDL_Scancode key;
		enum ControllerButton cbutton;
		enum MouseButton mbutton;
	};
	enum EventType type;
};

struct InputHandler
{
	Vec(void*) controllers;
	Vec(struct Event) events;
	const uint8_t* keystate;
	uint32_t mousestate;
	int mousex, mousey;
};

struct InputHandler* inputhandler_ctor(struct InputHandler* self);
int inputhandler_cjoystickangle(
	struct InputHandler* self, 
	int controller,
	enum ControllerJoystick joystick,
	double* angle
);
int inputhandler_cgetaxis(
	struct InputHandler* self, 
	int controller, 
	enum ControllerAxis axis
);
int inputhandler_cbuttondown(
	struct InputHandler* self, 
	int controller, 
	enum ControllerButton button
);
void inputhandler_cvibrate(
	struct InputHandler* self, 
	int controller, 
	size_t duration
);
void inputhandler_update(struct InputHandler* self);
void inputhandler_dtor(struct InputHandler* self);

#endif
