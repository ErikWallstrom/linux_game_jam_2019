#include "texture.h"
#include "font.h"
#include "log.h"
#include "window.h"

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wduplicated-branches"
#pragma GCC diagnostic ignored "-Wunused-but-set-variable"
#pragma GCC diagnostic ignored "-Wdouble-promotion"
#define STB_IMAGE_IMPLEMENTATION
#define STBI_FAILURE_USERMSG
#include "stb_image.h"
#pragma GCC diagnostic pop

#include <SDL2/SDL_ttf.h>

struct Texture* texture_ctorimage(
	struct Texture* self, 
	const char* path,
	struct Window* window)
{
	log_assert(self, "is NULL");
	log_assert(path, "is NULL");
	log_assert(window, "is NULL");

	//RGBA
	uint8_t* image = stbi_load(path, &self->width, &self->height, NULL, 4); 
	if(!image)
	{
		log_error("Failed to load image '%s': %s", path, stbi_failure_reason());
	}

	const uint32_t format = SDL_PIXELFORMAT_RGBA32;
	const int depth = 32;
	const int pitch = 4 * self->width;
	SDL_Surface* surface = SDL_CreateRGBSurfaceWithFormatFrom(
		image, 
		self->width, 
		self->height,
		depth,
		pitch,
		format
	);
	SDL_Renderer* renderer = window->renderer;
	self->raw = SDL_CreateTextureFromSurface(renderer, surface);
	if(!self->raw)
	{
		log_error("%s", SDL_GetError());
	}

	SDL_FreeSurface(surface);
	stbi_image_free(image);
	return self;
}

struct Texture* texture_ctortext(
	struct Texture* self, 
	struct Font* font, 
	const char* text,
	struct Window* window)
{
	log_assert(self, "is NULL");
	log_assert(font, "is NULL");
	log_assert(text, "is NULL");
	log_assert(window, "is NULL");

	SDL_Renderer* renderer = window->renderer;
	int lineheight = TTF_FontLineSkip(font->raw);
	self->width = 0;
	self->height = lineheight;

	int newwidth = -1;
	for(size_t i = 0; i < strlen(text); i++)
	{
		if(text[i] >= ' ' && text[i] <= '~')
		{
			if(newwidth != -1)
			{
				newwidth += font->atlas[text[i] - ' '].width;
				if(newwidth > self->width)
				{
					self->width = newwidth;
					newwidth = -1;
				}
			}
			else
			{
				self->width += font->atlas[text[i] - ' '].width;
			}
		}
		else if(text[i] == '\t')
		{
			//atlas[0] is space character. Tab = 4 spaces
			if(newwidth != -1)
			{
				newwidth += font->atlas[0].width * 4;
				if(newwidth > self->width)
				{
					self->width = newwidth;
					newwidth = -1;
				}
			}
			else
			{
				self->width += font->atlas[0].width * 4;
			}
		}
		else if(text[i] == '\n')
		{
			self->height += lineheight;
			newwidth = 0;
		}
		else
		{
			log_warning("'%c' is not renderable <%s>", text[i], __func__);
		}
	}

	self->raw = SDL_CreateTexture(
		renderer, 
		font->pixelformat, 
		SDL_TEXTUREACCESS_TARGET,
		self->width,
		self->height
	);

	if(!self->raw)
	{
		log_error("%s", SDL_GetError());
	}

	//NOTE: Line below makes transparency work for two textures on eachother
	SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);
	SDL_SetTextureBlendMode(self->raw, SDL_BLENDMODE_BLEND);
	SDL_SetRenderTarget(renderer, self->raw);
	SDL_RenderClear(renderer);

	int x = 0;
	int y = 0;

	for(size_t i = 0; i < strlen(text); i++)
	{
		if(text[i] >= ' ' && text[i] <= '~')
		{
			int width = font->atlas[text[i] - ' '].width;
			int height = font->atlas[text[i] - ' '].height;

			SDL_RenderCopy(
				renderer, 
				font->atlas[text[i] - ' '].raw, 
				NULL, 
				&(SDL_Rect){.x = x, .y = y, .w = width, .h = height}
			);

			x += width;
		}
		else if(text[i] == '\t')
		{
			x += font->atlas[0].width * 4;
		}
		else if(text[i] == '\n')
		{
			y += lineheight;
			x = 0;
		}
	}

	SDL_SetRenderTarget(renderer, NULL);
	return self;
}

struct Texture* texture_copy(
	struct Texture* self, 
	struct Texture* dest, 
	struct Window* window)
{
	log_assert(self, "is NULL");
	log_assert(dest, "is NULL");
	log_assert(window, "is NULL");

	SDL_Renderer* renderer = window->renderer;
	Uint32 format;
	SDL_QueryTexture(
		self->raw,
		&format, 
		NULL, 
		NULL, 
		NULL
	);

	dest->width = self->width;
	dest->height = self->height;
	dest->raw = SDL_CreateTexture(
		renderer, 
		format, 
		SDL_TEXTUREACCESS_TARGET,
		dest->width,
		dest->height
	);

	if(!dest->raw)
	{
		log_error("%s", SDL_GetError());
	}

	//NOTE: Line below makes transparency work for two textures on eachother
	SDL_SetTextureBlendMode(dest->raw, SDL_BLENDMODE_BLEND);
	SDL_SetRenderTarget(renderer, dest->raw);
	SDL_RenderCopy(
		renderer, 
		self->raw,
		&(SDL_Rect){0, 0, dest->width, dest->height},
		&(SDL_Rect){0, 0, dest->width, dest->height}
	);
	SDL_SetRenderTarget(renderer, NULL);
	return dest;
}

void texture_setcolor(struct Texture* self, struct Color color)
{
	log_assert(self, "is NULL");
	self->color = color;
	SDL_SetTextureColorMod(self->raw, color.r, color.g, color.b);
}

void texture_dtor(struct Texture* self)
{
	log_assert(self, "is NULL");
	SDL_DestroyTexture(self->raw);
}

